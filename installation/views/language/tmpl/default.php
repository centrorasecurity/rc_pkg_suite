<?php
/**
 * @package		Joomla.Installation
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<div class="panel panel-default">
    <div class="panel-heading white-bg">
		<h3 class="panel-title"><?php echo JText::_('INSTL_LANGUAGE_TITLE'); ?></h3>
    </div>
    <div class="panel-controls-buttons">
	<?php if ($this->document->direction == 'ltr') : ?>
		<button onclick="Install.submitform();" rel="next" title="<?php echo JText::_('JNext'); ?>" class="btn btn-success btn-sm mr5 mb10"><?php echo JText::_('JNext'); ?></button>
	<?php elseif ($this->document->direction == 'rtl') : ?>
		<button onclick="Install.submitform();" rel="next" title="<?php echo JText::_('JNext'); ?>" class="btn btn-success btn-sm mr5 mb10"><?php echo JText::_('JNext'); ?></button>
	<?php endif; ?>
	</div>
</div>
<form action="index.php" method="post" id="adminForm" class="form-validate">
	<div class="panel panel-success">
		<div class="panel-heading">
			<h4 class="panel-title"><?php echo JText::_('INSTL_SELECT_LANGUAGE_TITLE'); ?></h4>
        </div>
		<div class="m">
			<div class="install-text">
				<?php echo JText::_('INSTL_SELECT_LANGUAGE_DESC'); ?>
			</div>
			<div class="install-body">
				<div class="m">
					<fieldset>
						<?php echo $this->form->getInput('language'); ?>
					</fieldset>
				</div>
				<div class="clr"></div>
			</div>
			<div class="clr"></div>
		</div>
	</div>
	<input type="hidden" name="task" value="setup.setlanguage" />
	<?php echo JHtml::_('form.token'); ?>
</form>
