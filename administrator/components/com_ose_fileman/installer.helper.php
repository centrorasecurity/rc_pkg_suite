<?php
/**
  * @version     4.0 +
  * @package       Open Source Excellence Security Suite
  * @subpackage    Open Source Excellence File Manager
  * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
  * @author        Created on 23-Apr-2012
  * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  *
  *
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  *  @Copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
*/
defined('_JEXEC') or die("Direct Access Not Allowed");
class oseInstallerHelper
{
	var $backendPath;
	var $frontendPath;
	var $cpuFile;
	var $successStatus;
	var $failedStatus;
	var $notApplicable;
	var $totalStep;
	var $pageTitle;
	var $verifier;
	var $dbhelper;
	var $template;
	var $component;
	var $frontendCPUPath;
	var $backendCPUPath;
	function __construct()
	{
		jimport('joomla.application.component.controller');
		jimport('joomla.application.component.model');
		jimport('joomla.installer.installer');
		jimport('joomla.installer.helper');
		jimport('joomla.filesystem.file');
		jimport('joomla.filesystem.folder');
		jimport('joomla.filesystem.archive');
		jimport('joomla.filesystem.path');
		$this->component= 'com_ose_fileman';
		$this->com_title= OSEFILEMANTITLE. '-'. OSEFILEMANVERSION;
		$this->cpuFile = 'cpuCRT.zip';
		$this->backendPath= JPATH_ROOT.DS.'administrator'.DS.'components'.DS.$this->component.DS;
		$this->frontendPath= JPATH_ROOT.DS.'components'.DS.$this->component.DS;
		$this->frontendCPUPath= JPATH_ROOT.DS.'components'.DS.'com_ose_cpu'.DS;
		$this->backendCPUPath= JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_ose_cpu'.DS;
		$this->successStatus= '<div style="float:left;">.....&nbsp;</div><div style="color:#009900;">'.JText :: _('Installation completed').'</div><div style="clear:both;"></div>';
		$this->failedStatus= '<div style="float:left;">.....&nbsp;</div><div style="color:red;">'.JText :: _('Installation failed').'</div><div style="clear:both;"></div>';
		$this->notApplicable= '<div style="float:left;">.....&nbsp;</div><div>'.JText :: _('Installation not applicable').'</div><div style="clear:both;"></div>';
		$this->totalStep= 5;
		require_once(dirname(__FILE__).DS.'installer.template.php');
		$this->verifier= new oseInstallerVerifier();
		$this->template= new oseInstallerTemplate();
	}
	function install()
	{
		//check php version
		$installedPhpVersion= floatval(phpversion());
		$supportedPhpVersion= 5.2;
		require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.$this->component.DS.'installer.template.php');
		$step= JRequest :: getVar('step', '', 'post');
		$helper= new oseInstallerHelper;
		if($installedPhpVersion < $supportedPhpVersion)
		{
			$html= oseInstallerHelper :: getErrorMessage(101, $installedPhpVersion);
			$status= false;
			$nextstep= 0;
			$title= JText :: _('OSE Installer for').' '.$this->com_title;
			$install= 1;
			$substep= 0;
		}
		else
		{
			if(!empty($step))
			{
				$progress= $helper->installSteps($step);
				$html= $progress->message;
				$status= $progress->status;
				$nextstep= $progress->step;
				$title= $progress->title;
				$install= $progress->install;
				$substep= isset($progress->substep) ? $progress->substep : 0;
			}
			else
			{
				$nextstep= 1;
				$verifier= new oseInstallerVerifier();
				$imageTest= $verifier->testImage();
				$template= new oseInstallerTemplate();
				$html= $template->getHTML('welcome', $imageTest);
				$status= true;
				$title= JText :: _('OSE Installer for').' '.$this->com_title;
				$install= 1;
				$substep= 0;
			}
		}
		$this->template->cInstallDraw($html, $nextstep, $title, $status, $install, $substep);
		return;
	}
	function installSteps($step= 1)
	{
		$db= JFactory :: getDBO();
		switch($step)
		{
			case 1 :
				//check requirement
				$status= $this->checkRequirement(2);
				break;
			case 2 :
				//install backend system
				$status= $this->installBackend(3);
				break;
			case 3 :
				//install ajax system
				$status= $this->installCOMCPU(4);
				break;
			case 4 :
				//install frontend system
				$status= $this->installFrontend(5);
				break;
			case 5 :
				//install template
				$status= $this->prepareDatabase(6);
				break;
			case 6 :
			case 'UPDATE_DB' :
				//prepare database
				$status= $this->updateDatabase(7);
				break;
			case 7 :
				$status= $this->installPlugin(8);
				break;
			case 8 :
				$status= $this->installViews(9);
				break;
			case 9 :
				$status= $this->clearInstallation(100);
				break;
			case 100 :
				//show success message
				$status= $this->installationComplete(0);
				break;
			default :
				$status= new stdClass();
				$status->message= $this->getErrorMessage(0, '0a');
				$status->step= '-99';
				$status->title= JText :: _('OSE INSTALLER');
				$status->install= 1;
				break;
		}
		return $status;
	}
	function checkRequirement($step)
	{
		$status= true;
		$this->pageTitle= JText :: _('Checking Requirements');
		$html= '';
		$html .= '<div style="width:300px; float:left;">'.JText :: _('BACKEND ARCHIVE').'</div>';
		if(!$this->verifier->checkFileExist($this->backendPath.'admin.zip'))
		{
			$html .= $this->failedStatus;
			$status= false;
			$errorCode= '1a';
		}
		else
		{
			$html .= $this->successStatus;
		}
		$html .= '<div style="width:300px; float:left;">'.JText :: _('OSE CPU Backend ARCHIVE').'</div>';
		if(!$this->verifier->checkFileExist($this->backendPath.'com_cpu_admin.zip'))
		{
			$html .= $this->failedStatus;
			$status= false;
			$errorCode= '1b';
		}
		else
		{
			$html .= $this->successStatus;
		}
		$html .= '<div style="width:300px; float:left;">'.JText :: _('OSE CPU FRONTEND ARCHIVE').'</div>';
		if(!$this->verifier->checkFileExist($this->backendPath.'com_cpu_site.zip'))
		{
			$html .= $this->failedStatus;
			$status= false;
			$errorCode= '1b';
		}
		else
		{
			$html .= $this->successStatus;
		}
		/*
		$html .= '<div style="width:300px; float:left;">'.JText :: _('OSE FRONTEND ARCHIVE').'</div>';
		if(!$this->verifier->checkFileExist($this->frontendPath.'site.zip'))
		{
			$html .= $this->failedStatus;
			$status= false;
			$errorCode= '1c';
		}
		else
		{
			$html .= $this->successStatus;
		}
		
		$html .= '<div style="width:300px; float:left;">'.JText :: _('OSE CORE PLUGIN ARCHIVE').'</div>';
		if(!$this->verifier->checkFileExist($this->backendPath.'ose_plugins.zip'))
		{
			$html .= $this->failedStatus;
			$status= false;
			$errorCode= '1e';
		}
		else
		{
			$html .= $this->successStatus;
		}
		*/
		if($status)
		{
			$autoSubmit= $this->getAutoSubmitFunction();
			//$form = $this->getInstallForm(2);
			$message= $autoSubmit.$html;
		}
		else
		{
			$errorMsg= $this->getErrorMessage(1, $errorCode);
			$message= $html.$errorMsg;
			$step= $step -1;
		}
		$drawdata= new stdClass();
		$drawdata->message= $message;
		$drawdata->status= $status;
		$drawdata->step= $step;
		$drawdata->title= JText :: _('OSE CHECKING REQUIREMENT');
		$drawdata->install= 1;
		return $drawdata;
	}
	function getAutoSubmitFunction()
	{
		ob_start();
?>
		<script type="text/javascript">
		var i=3;

		function countDown()
		{
			if(i >= 0)
			{
				document.getElementById("timer").innerHTML = i;
				i = i-1;
				var c = window.setTimeout("countDown()", 1000);
			}
			else
			{
				document.getElementById("div-button-next").removeAttribute("onclick");
				document.getElementById("input-button-next").setAttribute("disabled","disabled");
				document.forms["installform"].submit();
			}
		}

		window.addEvent('domready', function() {
			countDown();
		});

		</script>
		<?php

		$autoSubmit= ob_get_contents();
		@ ob_end_clean();
		return $autoSubmit;
	}
	function installBackend($step)
	{
		$html= '';
		$html .= '<div style="width:300px; float:left;">'.JText :: _('OSE BACKEND INSTALLATION').'</div>';
		$zip= $this->backendPath.'admin.zip';
		$destination= $this->backendPath;
		if($this->extractArchive($zip, $destination))
		{
			$html .= $this->successStatus;
			$autoSubmit= $this->getAutoSubmitFunction();
			$message= $autoSubmit.$html;
			$status= true;
		}
		else
		{
			$html .= $this->failedStatus;
			$errorMsg= $this->getErrorMessage($step, $step);
			$message= $html.$errorMsg;
			$status= false;
			$step= $step -1;
		}
		
		$drawdata= new stdClass();
		$drawdata->message= $message;
		$drawdata->status= $status;
		$drawdata->step= $step;
		$drawdata->title= JText :: _('OSE BACKEND INSTALLATION');
		$drawdata->install= 1;
		return $drawdata;
	}
	function installCOMCPU($step)
	{
		$html= '';
		jimport('joomla.filesystem.file');
		$html .= '<div style="width:300px; float:left;">'.JText :: _('OSE CPU BACKEND INSTALLATION').'</div>';
		$zip= $this->backendPath.'com_cpu_admin.zip';
		$destination= JPATH_ADMINISTRATOR.DS.'components'.DS.'com_ose_cpu'.DS;
		if($this->extractArchive($zip, $destination))
		{
			$html .= $this->successStatus;
			$autoSubmit= $this->getAutoSubmitFunction();
			$message= $autoSubmit.$html;
			$status= true;
		}
		else
		{
			$html .= $this->failedStatus;
			$errorMsg= $this->getErrorMessage(2, '2');
			$message= $html.$errorMsg;
			$status= false;
			$step= $step -1;
		}
		if (JFile::exists($this->frontendCPUPath.'extjs'.DS.'init'))
		{
			JFile::delete($this->frontendCPUPath.'extjs'.DS.'init');
		}
		if (JFile::exists($this->backendCPUPath.'payment2'.DS.'gateway'.DS.'authorize'))
		{
			JFile::delete($this->backendCPUPath.'payment2'.DS.'gateway'.DS.'authorize');
		}
		$html .= '<div style="width:300px; float:left;">'.JText :: _('OSE CPU FRONTEND INSTALLATION').'</div>';
		$zip= $this->backendPath.'com_cpu_site.zip';
		$destination= JPATH_SITE.DS.'components'.DS.'com_ose_cpu'.DS;
		if($this->extractArchive($zip, $destination))
		{
			$html .= $this->successStatus;
			$autoSubmit= $this->getAutoSubmitFunction();
			$message= $autoSubmit.$html;
			$status= true;
		}
		else
		{
			$html .= $this->failedStatus;
			$errorMsg= $this->getErrorMessage(2, '2');
			$message= $html.$errorMsg;
			$status= false;
			$step= $step -1;
		}
		$drawdata= new stdClass();
		$drawdata->message= $message;
		$drawdata->status= $status;
		$drawdata->step= $step;
		$drawdata->title= JText :: _('OSE INSTALLING Central Processing Units');
		$drawdata->install= 1;
		return $drawdata;
	}
	function installFrontend($step)
	{
		$html= '';
		// Language ;
		$html .= '<div style="width:300px; float:left;">'.JText :: _('NO FRONTEND FILES APPLICABLE').'</div>';
		$html .= $this->successStatus;
		$autoSubmit= $this->getAutoSubmitFunction();
		$message= $autoSubmit.$html;
		$status= true;
		
		$drawdata= new stdClass();
		$drawdata->message= $message;
		$drawdata->status= $status;
		$drawdata->step= $step;
		$drawdata->title= JText :: _('INSTALLING FRONTEND SYSTEM');
		$drawdata->install= 1;
		return $drawdata;
	}
	function installPlugin($step)
	{
		$html= '';
		jimport('joomla.filesystem.file');
		$html .= '<div style="width:300px; float:left;">'.JText :: _('NO PLIGINS APPLICABLE').'</div>';
		
		$html .= $this->successStatus;
		$autoSubmit= $this->getAutoSubmitFunction();
		$message= $autoSubmit.$html;
		$status= true;
		
		$drawdata= new stdClass();
		$drawdata->message= $message;
		$drawdata->status= true;
		$drawdata->step= $step;
		$drawdata->title = JText :: _('INSTALLING PLUGINS');
		$drawdata->install= 1;
		return $drawdata;
	}
	function prepareDatabase($step)
	{
		$html= '';
		$html .= '<div style="width:300px; float:left;">'.JText :: _('Creating Database').'</div>';
		$queryResult= $this->installSQL();
		if($queryResult == true)
		{
			$html .= $this->successStatus;
			$autoSubmit= $this->getAutoSubmitFunction();
			//$form = $this->getInstallForm(7);
			$message= $autoSubmit.$html;
			$status= true;
		}
		else
		{
			$html .= $this->failedStatus;
			$errorMsg= $this->getErrorMessage(6, $queryResult);
			$message= $html.$errorMsg;
			$status= false;
			$step= $step -1;
		}
		$drawdata= new stdClass();
		$drawdata->message= $message;
		$drawdata->status= $status;
		$drawdata->step= $step;
		$drawdata->title= JText :: _('PREPARING DATABASE');
		$drawdata->install= 1;
		return $drawdata;
	}
	function UpdateDatabase($step)
	{
		$html= '';
		$html .= '<div style="width:300px; float:left;">'.JText :: _('Fix Database Integrity').'</div>';
		$queryResult= $this->fixIntegrity();
		if($queryResult == true)
		{
			$html .= $this->successStatus;
			$autoSubmit= $this->getAutoSubmitFunction();
			$message= $autoSubmit.$html;
			$status= true;
		}
		else
		{
			$html .= $this->failedStatus;
			$errorMsg= $this->getErrorMessage(7, $queryResult);
			$message= $html.$errorMsg;
			$status= false;
			$step= $step -1;
		}
		$drawdata= new stdClass();
		$drawdata->message= $message;
		$drawdata->status= $status;
		$drawdata->step= $step;
		$drawdata->title= JText :: _('Fixing Database Integrity');
		$drawdata->install= 1;
		return $drawdata;
	}
	function extractArchive($source, $destination)
	{
		// Cleanup path;
		$destination= JPath :: clean($destination);
		$source= JPath :: clean($source);
		$result= JArchive :: extract($source, $destination);
		if($result === false)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	function fixIntegrity()
	{
		$db= JFactory :: getDBO();
		return true;
	}
	function installSQL()
	{
		//-- common images
		$img_OK= '<img src="images/publish_g.png" />';
		$img_WARN= '<img src="images/publish_y.png" />';
		$img_ERROR= '<img src="images/publish_r.png" />';
		$BR= '<br />';
		//--install...
		$db= JFactory :: getDBO();
		return true;
	}
	function oseunpack($src, $dest, $file)
	{
		$extractdir= JPath :: clean($dest);
		$archivename= JPath :: clean($src.DS.$file);
		// do the unpacking of the archive
		$result= JArchive :: extract($archivename, $extractdir);
		if($result === false)
		{
			return false;
		}
		else
		{
			if(JFile :: delete($archivename))
			{
				return true;
			}
		}
	}
	function installLanguage($type)
	{
		if ($type =='back')
		{
			$src= JPATH_ADMINISTRATOR.DS.'components'.DS.$this->component.DS.'language'.DS.'en-GB'.DS.'en-GB.'.$this->component.'.ini';
			$dest= JPATH_ADMINISTRATOR.DS.'language'.DS.'en-GB'.DS.'en-GB.'.$this->component.'.ini';
		}
		elseif($type =='backsys')
		{
			$src= JPATH_ADMINISTRATOR.DS.'components'.DS.$this->component.DS.'language'.DS.'en-GB'.DS.'en-GB.'.$this->component.'.sys.ini';
			$dest= JPATH_ADMINISTRATOR.DS.'language'.DS.'en-GB'.DS.'en-GB.'.$this->component.'.sys.ini';
		}
		else
		{
			$src= JPATH_SITE.DS.'components'.DS.$this->component.DS.'language'.DS.'en-GB'.DS.'en-GB.'.$this->component.'.ini';
			$dest= JPATH_SITE.DS.'language'.DS.'en-GB'.DS.'en-GB.'.$this->component.'.ini';
		}
		if(!JFile :: copy($src, $dest))
		{
			echo JText :: _('Unable to copy language file');
			return false;
		}
		else
		{
			return true;
		}
	}
	function installMenuPatch()
	{
		return true;
	}
	function installModulePatch()
	{

			return true;
	}
	function installViews($step)
	{
		$html= '';
		$html .= '<div style="width:100px; float:left;">'.JText :: _('No Views to install for this component').'</div>';
		$result= null;
		$db= JFactory :: getDBO();

		$result= true;
		$viewhtml= '';

		if($result == true)
		{
			$html .= $this->successStatus;
			$autoSubmit= $this->getAutoSubmitFunction();
			//$form = $this->getInstallForm(5);
			$message= $autoSubmit.$html;
			$status= true;
		}
		else
		{
			$html .= $this->failedStatus;
			$html .= $viewhtml;
			$errorMsg= $this->getErrorMessage($step, $step);
			$message= $html.$errorMsg;
			$status= false;
			$step= $step -1;
		}
		$drawdata= new stdClass();
		$drawdata->message= $message;
		$drawdata->status= true;
		$drawdata->step= $step;
		$drawdata->title= JText :: _('CREATING VIEWS');
		$drawdata->install= 1;
		return $drawdata;
	}
	function clearInstallation($step)
	{
		jimport('joomla.filesystem.file');
		jimport('joomla.filesystem.folder');
		$html= '';
		$zip= array();
		$zip[]= $this->backendPath.'admin.zip';
		$zip[]= $this->backendPath.'com_cpu_admin.zip';
		$zip[]= $this->backendPath.'com_cpu_site.zip';
		$result= true;
		foreach($zip as $z)
		{
			$html .= '<div style="width:500px; float:left;">'.JText :: _('Clearing file').' '.$z.'</div>';
			$result= JFile :: delete($z);
			if($result == true)
			{
				$html .= $this->successStatus;
				$autoSubmit= $this->getAutoSubmitFunction();
				//$form = $this->getInstallForm(5);
				$message= $autoSubmit.$html;
				$status= true;
			}
			else
			{
				$html .= $this->failedStatus;
				$errorMsg= $this->getErrorMessage($step, $step);
				$message= $html.$errorMsg;
				$status= false;
				$step= $step -1;
			}
		}
		
		$drawdata= new stdClass();
		$drawdata->message= $message;
		$drawdata->status= true;
		$drawdata->step= $step;
		$drawdata->title= JText :: _('CLEARING INSTALLATION FILES');
		$drawdata->install= 1;
		return $drawdata;
	}
	function installationComplete($step)
	{
		$cache= JFactory :: getCache();
		$cache->clean();
		$version= OSEFILEMANVERSION;
		$file= dirname(__FILE__).DS.'installer.dummy.ini';
		if(JFile :: exists($file) && JFile :: delete($file))
		{
			$html= '';
			$html .= '<div style="margin: 30px 0; padding: 10px; background: #edffb7; border: solid 1px #8ba638; width: 50%; -moz-border-radius: 5px; -webkit-border-radius: 5px;">
									<div style="background: #edffb7 url(components/com_ose_cpu/assets/images/accept.png) no-repeat 0 0;width: 16px; height: 16px; float: left; margin-right: 10px;"></div>
									<h3 style="padding: 0; margin: 0 0 5px;">Installation has been completed</h3></div>';
		}
		else
		{
			$html= '<div></div>';
			$html .= '<div style="margin: 30px 0; padding: 10px; background: #edffb7; border: solid 1px #8ba638; width: 50%; -moz-border-radius: 5px; -webkit-border-radius: 5px;">
									<div style="background: #edffb7 url(components/com_ose_cpu/assets/images/accept.png) no-repeat 0 0;width: 16px; height: 16px; float: left; margin-right: 10px;"></div>
									<h3 style="padding: 0; margin: 0 0 5px;">Installation has been completed</h3>However we were unable to remove the file <b>installer.dummy.ini</b> located in the '.dirname(__FILE__).' folder. Please remove it manually in order to completed the installation.</div>';
		}
		ob_start();
?>
			<div style="margin: 30px 0; padding: 10px; background: #fbfbfb; border: solid 1px #ccc; width: 50%; -moz-border-radius: 5px; -webkit-border-radius: 5px;" >
			<h3 style="color: red;">Congratulations!</h3>
			<div>Installation has been completed, please click the next button to start using OSE File Manager</div>
		</div>

	<?php

		$content= ob_get_contents();
		ob_end_clean();
		$html .= $content;
		//$form = $this->getInstallForm(0, 0);
		$message= $html;
		$drawdata= new stdClass();
		$drawdata->message= $message;
		$drawdata->status= true;
		$drawdata->step= $step;
		$drawdata->title= JText :: _('INSTALLATION COMPLETED');
		$drawdata->install= 0;
		return $drawdata;
	}
	function getErrorMessage($error= "", $extraInfo= "")
	{
		switch($error)
		{
			case 0 :
				$errorWarning= $error.'-'.$extraInfo.' : '.JText :: _('The operation is invalid');
				break;
			case 1 :
				$errorWarning= $error.'-'.$extraInfo.' : '.JText :: _('The file is missing');
				break;
			case 2 :
				$errorWarning= $error.'-'.$extraInfo.' : '.JText :: _('OSE BACKEND EXTRACT FAILED WARN');
				break;
			case 3 :
				$errorWarning= $error.'-'.$extraInfo.' : '.JText :: _('OSE CPU INSTALL FAILED');
				break;
			case 4 :
				$errorWarning= $error.'-'.$extraInfo.' : '.JText :: _('OSE FRONTEND EXTRACT FAILED WARN');
				break;
			case 5 :
				$errorWarning= $error.'-'.$extraInfo.' : '.JText :: _('Error creating OSE tables');
				break;
			case 6 :
				$errorWarning= $error.'-'.$extraInfo.' : '.JText :: _('Error creating OSE tables');
				break;
			case 7 :
				$errorWarning= $error.'-'.$extraInfo.' : '.JText :: _('Error fixing OSE table integrity');
				break;
			case 8 :
				$errorWarning= $error.'-'.$extraInfo.' : '.JText :: _('Error creating Database Views');
				break;
			case 101 :
				$errorWarning= $error.' : '.JText :: _('PHP version is lower than 5.2, your version is'.' '.$extraInfo);
				break;
			default :
				$error=(!empty($error)) ? $error : '99';
				$errorWarning= $error.'-'.$extraInfo.' : '.JText :: _('UNEXPECTED ERROR WARN');
				break;
		}
		ob_start();
?>
		<div style="font-weight: 700; color: red; padding-top:10px">
			<?php echo $errorWarning; ?>
		</div>
		<div id="communityContainer" style="margin-top:10px">
			<div><?php echo JText::_('OSE INSTALLATION ERROR HELP'); ?></div>
			<div><a href="http://wiki.opensource-excellence.com/index.php?title=Trouble_Shooting_-_OSE_Membership_5">http://wiki.opensource-excellence.com/index.php?title=Trouble_Shooting_-_OSE_Membership_5</a></div>
		</div>
		<?php

		$errorMsg= ob_get_contents();
		@ ob_end_clean();
		return $errorMsg;
	}
}
class oseInstallerVerifier
{
	var $template;
	var $dbhelper;
	function __construct()
	{
		require_once(dirname(__FILE__).DS.'installer.template.php');
		$this->template= new oseInstallerTemplate();
	}
	function isLatestFriendTable()
	{
		return true;
	}
	function isLatestGroupMembersTable()
	{
		return true;
	}
	function isPhotoPrivacyUpdated()
	{
		return $this->dbhelper->checkPhotoPrivacyUpdated();
	}
	function isLatestGroupTable()
	{
		$fields= $this->dbhelper->_getFields();
		if(!array_key_exists('membercount', $fields))
		{
			return false;
		}
		if(!array_key_exists('wallcount', $fields))
		{
			return false;
		}
		if(!array_key_exists('discusscount', $fields))
		{
			return false;
		}
		return true;
	}
	/**
	 * Method to check if the GD library exist
	 *
	 * @returns boolean	return check status
	 **/
	function testImage()
	{
		$msg= '
							<style type="text/css">
							.Yes {
								color:#46882B;
								font-weight:bold;
							}
							.No {
								color:#CC0000;
								font-weight:bold;
							}
							.jomsocial_install tr {

							}
							.jomsocial_install td {
								color: #888;
								padding: 3px;
							}
							.jomsocial_install td.item {
								color: #333;
							}
							</style>
							<div class="install-body" style="background: #fbfbfb; border: solid 1px #ccc; -moz-border-radius: 5px; -webkit-border-radius: 5px; padding: 20px; width: 50%;">
								<p>If any of these items are not supported (marked as <span class="No">No</span>), your system does not meet the requirements for installation. Some features might not be available. Please take appropriate actions to correct the errors.</p>
									<table class="content jomsocial_install" style="width: 100%; background">
										<tbody>';
		// @rule: Test for JPG image extensions
		$type= 'JPEG';
		if(function_exists('imagecreatefromjpeg'))
		{
			$msg .= $this->template->testImageMessage($type, true);
		}
		else
		{
			$msg .= $this->template->testImageMessage($type, false);
		}
		// @rule: Test for png image extensions
		$type= 'PNG';
		if(function_exists('imagecreatefrompng'))
		{
			$msg .= $this->template->testImageMessage($type, true);
		}
		else
		{
			$msg .= $this->template->testImageMessage($type, false);
		}
		// @rule: Test for gif image extensions
		$type= 'GIF';
		if(function_exists('imagecreatefromgif'))
		{
			$msg .= $this->template->testImageMessage($type, true);
		}
		else
		{
			$msg .= $this->template->testImageMessage($type, false);
		}
		/*
		$type= 'FINFO_OPEN';
		if(function_exists('finfo_open'))
		{
			$msg .= $this->template->testImageMessage($type, true);
		}
		else
		{
			$msg .= $this->template->testImageMessage($type, false);
		}
		*/
		$msg .= '
										</tbody>
									</table>

							</div>';
		return $msg;
	}
	function checkFileExist($file)
	{
		return file_exists($file);
	}
}
?>