<?php
/**
 * @version     4.0 +
 * @package       Open Source Excellence Security Suite
 * @subpackage    Open Source Excellence File Manager
 * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
 * @author        Created on 23-Apr-2012
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 *
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  @Copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
 */

defined('_JEXEC') or die("Direct Access Not Allowed");
JHTML::_('behavior.modal');
// list directory contents
global $dir_up;
$dir = $_SESSION['nx_'.$GLOBALS['file_mode'].'dir'];

?>
<div id="oseheader">
	<div class="container">
		<div class="logo-labels">
			<h1>
				<a href="http://www.opensource-excellence.com" target="_blank"><?php echo JText::_("Open Source Excellence"); ?>
				</a>
			</h1>
		<?php
			echo $this->preview_menus; 
		?>
	</div>
<?php
	oseSoftHelper::showmenu();
?>
	<div class="section">
		<div id="sectionheader"><?php echo $this->title; ?></div>
		<div class="grid-title"><?php echo JText::_('Please manage your files here').'.'.JText::_("Your current location is").": ".$GLOBALS["home_dir"].DS.$dir; ?></div>
		<div id ='ose-directory-list'>
		
		<?php //Start ;// ?>		

		<div id="overDiv" style="position:absolute; visibility:hidden; z-index:10000;"></div>
		<?php
			$allow=($GLOBALS["permissions"]&01)==01;
			$admin=((($GLOBALS["permissions"]&04)==04) || (($GLOBALS["permissions"]&02)==02));
		
			$dir_up = dirname($dir);
			//print_r('<br/>$dir_up: '.$dir_up);
			if($dir_up==".") $dir_up = "";
		
			if(!get_show_item($dir_up,basename($dir))) show_error($dir." : ".$GLOBALS["error_msg"]["accessdir"]);
		
			// make file & dir tables, & get total filesize & number of items
			$dir_list = array();
			$file_list = array();
			$tot_file_size = 0;
			$num_items = 0;
			$this->model->make_tables($dir, $dir_list, $file_list, $tot_file_size, $num_items);
		
			$dirs = explode( "/", $dir );
			$implode = "";
			$dir_links = "<a href='".make_link( "directories", "", null )."'>..</a>/";
			foreach( $dirs as $directory ) {
			  if( $directory != "" ) {
				$implode .= $directory."/";
				$dir_links .= "<a href='".make_link( "directories", $implode, null )."'>$directory</a>/";
			  }
			}
			
			$this->model->show_header($GLOBALS["messages"]["actdir"].": ".$dir_links);
		
			// Javascript functions:
			//include OSEFILEMAN_B_PATH."/include/javascript.php";
		
			// Sorting of items
			$images = "&nbsp;<img width='10' height='10' border='0' align=\"absmiddle' src=\"".OSEFILEMAN_B_URL."/assets/images/";
			if($GLOBALS["srt"]=="yes") {
				$_srt = "no";	$images .= "_arrowup.gif\" alt=\"^\">";
			} else {
				$_srt = "yes";	$images .= "_arrowdown.gif\" alt=\"v\">";
			}
		
			// Toolbar
			echo "<br><tabl'width='100%' ><tr><td><table class='toolbar'><tr>\n";
		
			// PARENT DIR
			echo "<td width='10px'>";
			if( $dir != "" ) {
			  echo "<div class='toolbaricon' ><a href='".make_link("directories",$dir_up,NULL)."'>";
			  echo "<img border='0' width='16' height='16' align='absmiddle' src='".OSEFILEMAN_B_URL."/assets/images/arrow_turn_left.png' ";
			  echo "alt='".$GLOBALS["messages"]["uplink"]."' title='".$GLOBALS["messages"]["uplink"]."'></a></div>";
			}
			echo "</td>\n";
			// HOME DIR
			echo "<td width='10px'><div class='toolbaricon' ><a href='".make_link("directories",NULL,NULL)."'>";
			echo "<img border='0' width='16' height='16' align='absmiddle' src='".OSEFILEMAN_B_URL."/assets/images/home.png' ";
			echo "alt='".$GLOBALS["messages"]["homelink"]."' title='".$GLOBALS["messages"]["homelink"]."'></a></div></td>\n";
			// RELOAD
			echo "<td width='10px'><div class='toolbaricon' ><a href='javascript:location.reload();'><img border='0' width='16' height='16' ";
			echo "align='absmiddle' src='".OSEFILEMAN_B_URL."/assets/images/arrow_refresh.png' alt='".$GLOBALS["messages"]["reloadlink"];
			echo "' title='".$GLOBALS["messages"]["reloadlink"]."'></a></div></td>\n";
		 
		
			if($allow) {
				// COPY
				echo "<td  width='10px'><div class='toolbaricon' ><a href=\"#\" onclick =\"Copy()\" ><img border=\"0\" width=\"16\" height=\"16\" ";
				echo "align=\"absmiddle\" src=\"".OSEFILEMAN_B_URL."/assets/images/page_copy.png\" alt=\"".$GLOBALS["messages"]["copylink"];
				echo "\" title=\"".$GLOBALS["messages"]["copylink"]."\"></a></div></td>\n";
				// MOVE
				echo "<td width='10px'><div class='toolbaricon' ><a href=\"javascript:Move();\"><img border=\"0\" width=\"16\" height=\"16\" ";
				echo "align=\"absmiddle\" src=\"".OSEFILEMAN_B_URL."/assets/images/cut.png\" alt=\"".$GLOBALS["messages"]["movelink"];
				echo "\" title=\"".$GLOBALS["messages"]["movelink"]."\"></a></div></td>\n";
				// DELETE
				echo "<td width='10px'><div class='toolbaricon' ><a href=\"javascript:Delete();\"><img border=\"0\" width=\"16\" height=\"16\" ";
				echo "align=\"absmiddle\" src=\"".OSEFILEMAN_B_URL."/assets/images/delete.png\" alt=\"".$GLOBALS["messages"]["dellink"];
				echo "\" title=\"".$GLOBALS["messages"]["dellink"]."\"></a></div></td>\n";
				// UPLOAD
				if(ini_get("file_uploads")) {
					echo "<td width='10px'><div class='toolbaricon' ><a href=\"".make_link("upload",$dir,NULL)."\">";
					echo "<img border=\"0\" width=\"16\" height=\"16\" align=\"absmiddle\" ";
					echo "src=\"".OSEFILEMAN_B_URL."/assets/images/_upload.gif\" alt=\"".$GLOBALS["messages"]["uploadlink"];
					echo "\" title=\"".$GLOBALS["messages"]["uploadlink"]."\"></a></div></td>\n";
				} else {
					echo "<td width='10px'><div class='toolbaricon' ><img border=\"0\" width=\"16\" height=\"16\" align=\"absmiddle\" ";
					echo "src=\"".OSEFILEMAN_B_URL."/assets/images/_upload_.gif\" alt=\"".$GLOBALS["messages"]["uploadlink"];
					echo "\" title=\"".$GLOBALS["messages"]["uploadlink"]."\"></div></td>\n";
				}
				// ARCHIVE
				if( ($GLOBALS["zip"] || $GLOBALS["tar"] || $GLOBALS["tgz"]) && !nx_isFTPMode() ) {
					echo "<td width='10px'><div class='toolbaricon' ><a href=\"javascript:Archive();\"><img border=\"0\" width=\"16\" height=\"16\" ";
					echo "align=\"absmiddle\" src=\"".OSEFILEMAN_B_URL."/assets/images/archive.png\" alt=\"".$GLOBALS["messages"]["comprlink"];
					echo "\" title=\"".$GLOBALS["messages"]["comprlink"]."\"></a></div></td>\n";
				}
			} else {
				// COPY
				echo "<td width='10px'><div class='toolbaricon' ><img border=\"0\" width=\"16\" height=\"16\" align=\"absmiddle\" ";
				echo "src=\"".OSEFILEMAN_B_URL."/assets/images/_copy_.gif\" alt=\"".$GLOBALS["messages"]["copylink"]."\" title=\"";
				echo $GLOBALS["messages"]["copylink"]."\"></div></td>\n";
				// MOVE
				echo "<td width='10px'><div class='toolbaricon' ><img border=\"0\" width=\"16\" height=\"16\" align=\"absmiddle\" ";
				echo "src=\"".OSEFILEMAN_B_URL."/assets/images/_move_.gif\" alt=\"".$GLOBALS["messages"]["movelink"]."\" title=\"";
				echo $GLOBALS["messages"]["movelink"]."\"></div></td>\n";
				// DELETE
				echo "<td width='10px'><div class='toolbaricon' ><img border=\"0\" width=\"16\" height=\"16\" align=\"absmiddle\" ";
				echo "src=\"".OSEFILEMAN_B_URL."/assets/images/_delete_.gif\" alt=\"".$GLOBALS["messages"]["dellink"]."\" title=\"";
				echo $GLOBALS["messages"]["dellink"]."\"></div></td>\n";
				// UPLOAD
				echo "<td width='10px'><div class='toolbaricon' ><img border=\"0\" width=\"16\" height=\"16\" align=\"absmiddle\" ";
				echo "src=\"".OSEFILEMAN_B_URL."/assets/images/_upload_.gif\" alt=\"".$GLOBALS["messages"]["uplink"];
				echo "\" title=\"".$GLOBALS["messages"]["uplink"]."\"></div></td>\n";
			}
		
			if (file_exists(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_ose_cpu'.DS.'filescan'.DS.'filescan.php'))
			{
				
				echo "<td  width='10px'><div class='toolbaricon' ><a href=\"#\" onclick =\"Dbinit()\" ><img border=\"0\" width=\"16\" height=\"16\" ";
				echo "align=\"absmiddle\" src=\"".OSEFILEMAN_B_URL."/assets/images/dbinitialise.png\" alt=\"".$GLOBALS["messages"]["dbinitialise"];
				echo "\" title=\"".$GLOBALS["messages"]["dbinitialise"]."\"></a></div></td>\n";
				
				echo "<td  width='10px'><div class='toolbaricon' ><a href=\"#\" onclick =\"DbinitCustom()\" ><img border=\"0\" width=\"16\" height=\"16\" ";
				echo "align=\"absmiddle\" src=\"".OSEFILEMAN_B_URL."/assets/images/dbinitcustom.png\" alt=\"".$GLOBALS["messages"]["dbinitcustom"];
				echo "\" title=\"".$GLOBALS["messages"]["dbinitcustom"]."\"></a></div></td>\n";
				
				echo "<td  width='10px'><div class='toolbaricon' ><a href=\"#\" onclick =\"DbinitClear()\" ><img border=\"0\" width=\"16\" height=\"16\" ";
				echo "align=\"absmiddle\" src=\"".OSEFILEMAN_B_URL."/assets/images/dbinitclear.png\" alt=\"".$GLOBALS["messages"]["dbinitclear"];
				echo "\" title=\"".$GLOBALS["messages"]["dbinitclear"]."\"></a></div></td>\n";
				
				
			}
			// ADMIN & LOGOUT
			if($GLOBALS["require_login"]) {
				echo "<td width='10px'>::</td>";
				// ADMIN
				if($admin) {
					echo "<td width='10px'><a href=\"".make_link("admin",$dir,NULL)."\">";
					echo "<img border=\"0\" width=\"16\" height=\"16\" align=\"absmiddle\" ";
					echo "src=\"".OSEFILEMAN_B_URL."/assets/images/_admin.gif\" alt=\"".$GLOBALS["messages"]["adminlink"]."\" title=\"";
					echo $GLOBALS["messages"]["adminlink"]."\"></A></td>\n";
				}
				// LOGOUT
				echo "<td width='10px'><a href=\"".make_link("logout",NULL,NULL)."\">";
				echo "<img border=\"0\" width=\"16\" height=\"16\" align=\"absmiddle\" ";
				echo "src=\"".OSEFILEMAN_B_URL."/assets/images/_logout.gif\" alt=\"".$GLOBALS["messages"]["logoutlink"]."\" title=\"";
				echo $GLOBALS["messages"]["logoutlink"]."\"></a></td>\n";
			}
		
			// Create File / Dir
		
			if($allow && @$GLOBALS['nx_File']->is_writable( get_abs_dir( $dir)) ) {
				echo "<td align=\"right\">
						<table id='createitemtable'><tr><td>
							<select name=\"mktype\" id=\"mktype\" onchange=\"checkMkitemForm(this.options[this.selectedIndex])\">
								<option value=\"file\">".$GLOBALS["mimes"]["file"]."</option>
								<option value=\"dir\">".$GLOBALS["mimes"]["dir"]."</option>";
				if( !nx_isFTPMode() && !$GLOBALS['isWindows']) {
					echo "			<option value=\"symlink\">".$GLOBALS["mimes"]["symlink"]."</option>\n";
				}
				echo "		</select>
							<input name=\"symlink_target\" id=\"symlink_target\" type=\"hidden\" size=\"25\" title=\"{$GLOBALS['messages']['symlink_target']}\" value=\"".JPATH_BASE."\" />
							<input name=\"mkname\" id=\"mkname\" type=\"text\" size=\"15\" title=\"{$GLOBALS['messages']['nameheader']}\" />
							<button class='button' id ='createfile' name ='createfile' onclick = 'createfile()'>".$GLOBALS["messages"]["btncreate"]."</button>
							</td></tr>
							<tr><td id=\"quick_jumpto\">".
							//list_bookmarks( $dir ).
							"</td></tr>
						</table>
						<script type=\"text/javascript\">function checkMkitemForm( el ) { if( el.value =='symlink' ) document.mkitemform.symlink_target.type='text'; else document.mkitemform.symlink_target.type='hidden';} </script>
						<input type='hidden' name='dir' id='dir' value ='".$dir."'/>
					  </td>\n";
			}
			else {
				echo "<td align=\"right\">
						<table><tr><td id=\"quick_jumpto\">".
						//list_bookmarks( $dir ).
						"<input type='hidden' name='dir' id='dir' value ='".$dir."'/>".
						"</td></tr></table>
					 </td>";
			}
		
			echo "</tr></table>\n";
		
			// End Toolbar
		
		
			// Begin Table + Form for checkboxes
			echo "<form name=\"selform\" method=\"post\" action=\"".make_link("post",$dir,null)."\">
			<input type=\"hidden\" name=\"do_action\" /><input type=\"hidden\" name=\"first\" value=\"y\" />
			<table class=\"adminlist\" width=\"95%\">\n";
		
			if( extension_loaded( "posix" )) {
			  	$owner_info = '<th width="15%" class="title">' . $GLOBALS['messages']['miscowner'] . '&nbsp;';
			  	if( nx_isFTPMode() ) {
			  		$my_user_info = posix_getpwnam( $_SESSION['ftp_login'] );
			  		$my_group_info = posix_getgrgid( $my_user_info['gid'] );
			  	} else {
			  		if (function_exists('posix_getpwuid'))
			  		{
					$my_user_info = posix_getpwuid( posix_geteuid() );
					$my_group_info = posix_getgrgid(posix_getegid() );
			  		}
			  		else
			  		{
			  		$my_user_info = "UID: ".posix_geteuid();
					$my_group_info = "GID: ".posix_getegid();
			  		}
			  	}
				$owner_info .= mosTooltip( mysql_escape_string( sprintf( $GLOBALS['messages']['miscownerdesc'],  $my_user_info['name'], $my_user_info['uid'], $my_group_info['name'], $my_group_info['gid'] ))); // new [mic]
		
			  	$owner_info .= "</th>\n";
			  	$colspan=8;
			}
			else {
			  $owner_info = "";
			  $colspan = 7;
			}
			// Table Header
			echo "<tr>
			<th width='2%' class='title'>
				<input type='checkbox' name='toggleAllC' onclick='javascript:ToggleAll(this);' />
			</th>
			<th class='title'>\n";
			if($GLOBALS["order"]=="name") $new_srt = $_srt;	else $new_srt = "yes";
			echo "<a href='".make_link("directories",$dir,NULL,"name",$new_srt)."'>".$GLOBALS["messages"]["nameheader"];
			if($GLOBALS["order"]=="name") echo $images; echo '</a>';
			echo "</th>
			<th width='8%' class='title'>";
			if($GLOBALS["order"]=="size") $new_srt = $_srt;	else $new_srt = "yes";
			echo "<a href='".make_link("directories",$dir,NULL,"size",$new_srt)."'>".$GLOBALS["messages"]["sizeheader"];
			if($GLOBALS["order"]=="size") echo $images;
			echo "</a></th>
			<th width='10%' class='title'>";
			if($GLOBALS["order"]=="type") $new_srt = $_srt;	else $new_srt = "yes";
			echo "<a href='".make_link("directories",$dir,NULL,"type",$new_srt)."'>".$GLOBALS["messages"]["typeheader"];
			if($GLOBALS["order"]=="type") echo $images;
			echo "</a></th>
			<th width='14%' class='title'>";
			if($GLOBALS["order"]=="mod") $new_srt = $_srt;	else $new_srt = "yes";
			echo "<a href='".make_link("directories",$dir,NULL,"mod",$new_srt)."'>".$GLOBALS["messages"]["modifheader"];
			if($GLOBALS["order"]=="mod") echo $images;
			echo "</a></th>
			<th width='8%' class='title'>".$GLOBALS["messages"]["permheader"]."\n";
			echo "</th>";
			echo $owner_info;
			echo "<th width='10%' class='title'>".$GLOBALS["messages"]["actionheader"]."</th>
		
			</tr>\n";
		
			// make & print Table using lists
			$this->model->print_table($dir, $this->model->make_list($dir_list, $file_list), $allow);
		
			// print number of items & total filesize
			echo "<tr><td colspan='$colspan'><hr/></td></tr><tr>\n<td class='title'></td>";
			echo "<td class='title'>".$num_items." ".$GLOBALS["messages"]["miscitems"]." (";
		
			if(function_exists("disk_free_space")) {
				$size = disk_free_space($GLOBALS['home_dir']. $GLOBALS['separator']);
				$free=parse_file_size($size);
			}
			elseif(function_exists("diskfreespace")) {
				$size = diskfreespace($GLOBALS['home_dir'] . $GLOBALS['separator']);
				$free=parse_file_size($size);
			}
			else $free = "?";
		
			echo $GLOBALS["messages"]["miscfree"].": ".$free.")</td>\n";
			echo "<td class='title'>".parse_file_size($tot_file_size)."</td>\n";
			for($i=0;$i<($colspan-3);++$i) echo"<td class='title'></td>";
			echo "</tr>\n<tr><td colspan='$colspan'><hr/></td></tr></table>
				</form>";
	
	?><script type="text/javascript"><!--
		// Uncheck all items (to avoid problems with new items)
		var ml = document.selform;
		var len = ml.elements.length;
		for(var i=0; i<len; ++i) {
			var e = ml.elements[i];
			if(e.name == "selitems[]" && e.checked == true) {
				e.checked=false;
			}
		}
	// --></script>
		
		
<?php //Ends ;// ?>			
		
		
		
			<?php echo OSESoftHelper::getToken();  ?>
		</div>
	</div>
</div>
</div>

<?php
echo oseSoftHelper::renderOSETM();
?>