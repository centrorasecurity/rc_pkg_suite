Ext.ns('ose.Fileman');
ose.Fileman.msg = new Ext.App();

function ajaxAction(option, controller, task, dir, returnDir) {
	//Ext.MessageBox.wait('Please wait while file is being uploaded', 'Performing Actions');
	
	var formEl = Ext.get('ose-fileman-upload').query('input');
	var i = 0;
	var files = new Object(); 
	for (i=0; i<10; i++)
	{
		files[i]= formEl[i].value; 
	}
	files = Ext.encode(files);  
	
	Ext.Ajax.request({
		url : 'index.php',
		params : {
			option : option,
			task : task,
			controller : controller,
			dir: dir,
			files: files
		},
		method : 'POST',
		success : function(result, request) {
			msg = Ext.decode(result.responseText);
			if (msg.status != 'ERROR') {
				Ext.MessageBox.hide();
				Ext.Msg.alert(msg.title, msg.content, function(btn,txt)	{
					if(btn == 'ok')	{
						window.location = 'index.php?option=com_ose_fileman&view=directories&dir='+returnDir.getValue()+'&order=name&srt=yes'
               		}
				}); 
			} else {
				Ext.MessageBox.hide();
				ose.Fileman.msg.setAlert('Error', msg.content);
				
			}
		}
	});
}

Ext.onReady(function(){
	
var uploadbtn = Ext.get('uploadbtn');
var dir = Ext.get('dir');
var returnDir = Ext.get('returnDir');

uploadbtn.on('click', function(){
		ajaxAction('com_ose_fileman', 'files', 'upload_files', dir.getValue(), returnDir) ;
		
	});

var closebtn = Ext.get('closebtn');

closebtn.on('click', function(){
	window.location = 'index.php?option=com_ose_fileman&view=directories&dir='+returnDir.getValue()+'&order=name&srt=yes'
});
	
});	