Ext.ns('ose.Fileman');

function ajaxAction(option, task, controller, file, content) {
	Ext.MessageBox.wait('Please wait while file is being saved', 'Performing Actions');
	var i = 0;
	Ext.Ajax.request({
		url : 'index.php',
		params : {
			option : option,
			task : task,
			controller : controller,
			file: file, 
			code : content
		},
		method : 'POST',
		success : function(result, request) {
			msg = Ext.decode(result.responseText);
			if (msg.status != 'ERROR') {
				Ext.MessageBox.hide();
				ose.Fileman.msg.setAlert(msg.title, msg.content);
			} else {
				Ext.MessageBox.hide();
				ose.Fileman.msg.setAlert('Error', msg.content);
				
			}
		}
	});
}

Ext.onReady(function(){
	
var savebtn = Ext.get('savebtn');
var savefileName = Ext.get('savefileName');

ose.Fileman.msg = new Ext.App();

savebtn.on('click', function(){
		var te = window.editor.getValue();
		//te.value = te.value.replace(/^\s*/, "");
		ajaxAction('com_ose_fileman', 'save', 'edit', savefileName.dom.value, te) ; 
	});

var closebtn = Ext.get('closebtn');
var returnDir = Ext.get('returnDir');
closebtn.on('click', function(){
	window.location = 'index.php?option=com_ose_fileman&view=directories&dir='+returnDir.dom.value+'&order=name&srt=yes'
});


	
});	
