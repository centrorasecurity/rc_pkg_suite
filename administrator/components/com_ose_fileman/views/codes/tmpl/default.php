<?php
/**
 * @version     4.0 +
 * @package       Open Source Excellence Security Suite
 * @subpackage    Open Source Excellence File Manager
 * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
 * @author        Created on 23-Apr-2012
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 *
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  @Copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
 */

defined('_JEXEC') or die("Direct Access Not Allowed");

?>
		<div id ='ose-fileman-edit' height="90%">
		<table class="adminform" width="95%" height="95%">
			<tr>
			  <td>
				<div id="editorarea">
					<textarea disabled="disabled" class="<?php echo $this->fileType; ?>" style="width:95%;" name="codearea" id="codearea" rows="40" cols="120" wrap="off" >
						<?php echo $this->fileContent; ?>
					</textarea>
				</div>
				<br/>
			  </td>
			</tr>
		</table>
  
  	</div>
	

<script id="script">
window.onload = function() {
	var te = document.getElementById("codearea");
	//var sc = document.getElementById("script");
	te.value = (te.textContent || te.innerText || te.innerHTML).replace(/^\s*/, "");
	te.innerHTML = "";

	var foldFunc = CodeMirror.newFoldFunction(CodeMirror.braceRangeFinder);
	window.editor = CodeMirror.fromTextArea(te, {
		mode: "<?php echo $this->fileType; ?>",
		lineNumbers: true,
        matchBrackets: true,		
		lineWrapping: true,
        indentUnit: 4,
        indentWithTabs: true,
        enterMode: "keep",
        tabMode: "shift",
		onGutterClick: foldFunc,
		extraKeys: {
			"Ctrl-Q": function(cm){
				foldFunc(cm, cm.getCursor().line);
			}
		}
	});
};
</script>
