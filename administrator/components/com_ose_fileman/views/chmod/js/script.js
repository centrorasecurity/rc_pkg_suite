Ext.ns('ose.Fileman');
ose.Fileman.msg = new Ext.App();
function translate(tfvalue)
{
	if (tfvalue == true)
	{
		return 1;
	}
	else
	{
		return 0; 
	}
}
function ajaxAction(option, controller, task, dir, file, newperm, returnDir) {
	//Ext.MessageBox.wait('Please wait while file is being chmod', 'Performing Actions');
	var i = 0;
	var do_recurse = translate(Ext.get('do_recurse').dom.checked); 
	
	Ext.Ajax.request({
		url : 'index.php',
		params : {
			option : option,
			task : task,
			controller : controller,
			dir: dir,
			file: file, 
			newperm : newperm, 
			do_recurse: do_recurse
		},
		method : 'POST',
		success : function(result, request) {
			msg = Ext.decode(result.responseText);
			if (msg.status != 'ERROR') {
				Ext.MessageBox.hide();
				Ext.Msg.alert(msg.title, msg.content, function(btn,txt)	{
					if(btn == 'ok')	{
						window.location = 'index.php?option=com_ose_fileman&view=directories&dir='+returnDir.getValue()+'&order=name&srt=yes'
               		}
				}); 
			} else {
				Ext.MessageBox.hide();
				ose.Fileman.msg.setAlert('Error', msg.content);
				
			}
		}
	});
}



Ext.onReady(function(){
	
var changebtn = Ext.get('changebtn');
var fileName = Ext.get('file');

var dir = Ext.get('dir');
var returnDir = Ext.get('returnDir');

changebtn.on('click', function(){
	newperms =  '';
	newperms += translate(Ext.get('r0').dom.checked); 
	newperms += translate(Ext.get('w0').dom.checked);
	newperms += translate(Ext.get('x0').dom.checked);

	newperms += translate(Ext.get('r1').dom.checked);
	newperms += translate(Ext.get('w1').dom.checked);
	newperms += translate(Ext.get('x1').dom.checked);

	newperms += translate(Ext.get('r2').dom.checked);
	newperms += translate(Ext.get('w2').dom.checked);
	newperms += translate(Ext.get('x2').dom.checked);
	
	ajaxAction('com_ose_fileman', 'files', 'chmod_file', dir.getValue(), fileName.getValue(), newperms, returnDir) ;
		
	});

var closebtn = Ext.get('closebtn');

closebtn.on('click', function(){
	window.location = 'index.php?option=com_ose_fileman&view=directories&dir='+returnDir.getValue()+'&order=name&srt=yes'
});


	
});	
