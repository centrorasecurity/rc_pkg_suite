<?php
/**
  * @version     4.0 +
  * @package       Open Source Excellence Security Suite
  * @subpackage    Open Source Excellence File Manager
  * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
  * @author        Created on 23-Apr-2012
  * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  *
  *
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  *  @Copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
*/

// no direct access
defined('_JEXEC') or die(';)');
jimport('joomla.application.component.modeladmin');
class OSEFILEMANModelDirectories extends OSEFILEMANModel
{
	protected $lists= array(), $pagination= null;
	function __construct()
	{
		parent :: __construct();
	}
	
	
	function make_list($_list1, $_list2) {
		// make list of files
		$list = array();

		if($GLOBALS["srt"]=="yes") {
			$list1 = $_list1;
			$list2 = $_list2;
		} else {
			$list1 = $_list2;
			$list2 = $_list1;
		}

		if(is_array($list1)) {
			while (list($key, $val) = each($list1)) {
				$list[$key] = $val;
			}
		}

		if(is_array($list2)) {
			while (list($key, $val) = each($list2)) {
				$list[$key] = $val;
			}
		}

		return $list;
	}

	function make_tables($dir, &$dir_list, &$file_list, &$tot_file_size, &$num_items)
	{
		// make table of files in dir
		// make tables & place results in reference-variables passed to function
		// also 'return' total filesize & total number of items
		$homedir = realpath($GLOBALS['home_dir']);
		$tot_file_size = $num_items = 0;
		// Open directory

		$handle = @$GLOBALS['nx_File']->opendir(get_abs_dir($dir));

		if($handle===false && $dir=="") {
			$handle = @$GLOBALS['nx_File']->opendir($homedir . $GLOBALS['separator']);
		}

		if($handle===false)
		show_error($dir.": ".$GLOBALS["error_msg"]["opendir"]);

		// Read directory
		while(($new_item = @$GLOBALS['nx_File']->readdir($handle))!==false) {

			if( is_array( $new_item ))  {
				$abs_new_item = $new_item;
			} else {
				$abs_new_item = get_abs_item($dir, $new_item);
			}

			if ($new_item == "." || $new_item == "..") continue;

			if(!@$GLOBALS['nx_File']->file_exists($abs_new_item)) //show_error($dir."/$abs_new_item: ".$GLOBALS["error_msg"]["readdir"]);
			if(!get_show_item($dir, $new_item)) continue;

			$new_file_size = @$GLOBALS['nx_File']->filesize($abs_new_item);
			$tot_file_size += $new_file_size;
			$num_items++;
			$new_item_name = $new_item;
			if( nx_isFTPMode() ) {
				$new_item_name = $new_item['name'];
			}

			if(get_is_dir( $abs_new_item)) {

				if($GLOBALS["order"]=="mod") {
					$dir_list[$new_item_name] =
					@$GLOBALS['nx_File']->filemtime($abs_new_item);
				} else {	// order == "size", "type" or "name"

					$dir_list[$new_item_name] = $new_item;
				}
			} else {
				if($GLOBALS["order"]=="size") {
					$file_list[$new_item_name] = $new_file_size;
				} elseif($GLOBALS["order"]=="mod") {
					$file_list[$new_item_name] =
					@$GLOBALS['nx_File']->filemtime($abs_new_item);
				} elseif($GLOBALS["order"]=="type") {
					$file_list[$new_item_name] =
					get_mime_type( $abs_new_item, "type");
				} else {	// order == "name"
					$file_list[$new_item_name] = $new_item;
				}
			}
		}
		@$GLOBALS['nx_File']->closedir($handle);

		// sort
		if(is_array($dir_list)) {
			if($GLOBALS["order"]=="mod") {
				if($GLOBALS["srt"]=="yes") arsort($dir_list);
				else asort($dir_list);
			} else {	// order == "size", "type" or "name"
				if($GLOBALS["srt"]=="yes") ksort($dir_list);
				else krsort($dir_list);
			}
		}

		// sort
		if(is_array($file_list)) {
			if($GLOBALS["order"]=="mod") {
				if($GLOBALS["srt"]=="yes") arsort($file_list);
				else asort($file_list);
			} elseif($GLOBALS["order"]=="size" || $GLOBALS["order"]=="type") {
				if($GLOBALS["srt"]=="yes") asort($file_list);
				else arsort($file_list);
			} else {	// order == "name"
				if($GLOBALS["srt"]=="yes") ksort($file_list);
				else krsort($file_list);
			}
		}
	}
	
	
	function print_table($dir, $list, $allow) {
		// print table of files
		global $dir_up;
		if(!is_array($list)) return;
		if( $dir != "" || strstr( $dir, OSEFILEMAN_B_PATH ) ) {
			echo "<tr class=\"row1\">
		  			<td>&nbsp;</td>
		  			<td valign=\"baseline\">
		  				<a href=\"".make_link("directories",$dir_up,NULL)."\">
		  				<img border=\"0\" width=\"16\" height=\"16\" align=\"absmiddle\" src=\"".OSEFILEMAN_B_URL."/assets/images/arrow_turn_left.png\" alt=\"".$GLOBALS["messages"]["uplink"]."\" title=\"".$GLOBALS["messages"]["uplink"]."\"/>&nbsp;&nbsp;..</a>
		  			</td>
		  			<td>&nbsp;</td>
		  			<td>&nbsp;</td>
		  			<td>&nbsp;</td>
		  			<td>&nbsp;</td>
		  			<td>&nbsp;</td>";
			if( extension_loaded( "posix" )) {
				echo "<td>&nbsp;</td>";
			}
			echo "</tr>";
		}
		$i = 0;
		$toggle = false;
	
		while(list($item,$info) = each($list)){
			// link to dir / file
			if( is_array( $info )) {
				$abs_item=$info;
				if( extension_loaded('posix')) {
					$user_info = posix_getpwnam( $info['user']);
					$file_info['uid'] = $user_info['uid'];
					$file_info['gid'] = $user_info['gid'];
				}
			} else {
				$abs_item=get_abs_item($dir,$item);
				$file_info = @stat( $abs_item );
			}
	
			$is_writable = @$GLOBALS['nx_File']->is_writable( $abs_item );
			$is_chmodable = @$GLOBALS['nx_File']->is_chmodable( $abs_item );
			$is_readable =@$GLOBALS['nx_File']->is_readable( $abs_item );
			$is_deletable = @$GLOBALS['nx_File']->is_deletable( $abs_item );
	
			$target="";
			$extra="";
			if(@$GLOBALS['nx_File']->is_link($abs_item))  {
				$extra=" -> ".@readlink($abs_item);
			}
			if(@get_is_dir($abs_item, '')) {
				$link = make_link("directories",get_rel_item($dir, $item),NULL);
			} else {
				if(get_is_editable($abs_item) && $is_writable ) {
					$link = make_link( 'edit', $dir, $item);
				}
				elseif( $is_readable )  {
					if( strstr(get_abs_dir( $dir ), JPATH_SITE ) && !$GLOBALS['nx_File']->is_link($abs_item)) {
						$link = $GLOBALS["home_url"]."/".get_rel_item($dir, $item);
	
						// uwalter: Bugfix - image view admin folder issue.
						if (! preg_match('/index\.php/', $link)) {
							//$link = preg_replace('/(.+?)\/administrator(.*)/', '$1$2', $link);
							$link = preg_replace('/\/administrator/', '', $link);
						}
						$target = '_blank';
					} else {
						$link = make_link('download', $dir, $item );
					}
				}
			}
	
			if( nx_isIE() ) {
				echo '<tr onmouseover="style.backgroundColor=\'#D8ECFF\';" onmouseout="style.backgroundColor=\'#EAECEE\';" bgcolor=\'#EAECEE\'>';
			}
			else {
				$toggle = ($toggle) ? '1' : '0';
				echo "<tr class=\"row$toggle\">";
				$toggle = !$toggle;
			}
			echo "<td><input type=\"checkbox\" id=\"item_$i\" name=\"selitems[]\" value=\"";
			echo urlencode($item)."\" onclick=\"javascript:Toggle(this);\" /></td>\n";
			// Icon + Link
			echo "<td nowrap=\"nowrap\" align=\"left\">";
			if($is_readable) {
				echo"<a class='fmitems' href=\"".$link."\" target=\"".$target."\">";
			}
			//else echo "<<>";
			echo "<img border=\"0\" width=\"16\" height=\"16\" ";
	
			echo "align=\"absmiddle\" src=\"".OSEFILEMAN_B_URL."/assets/images/".get_mime_type($abs_item, "img")."\" alt=\"\" />&nbsp;";
			$s_item=$item;	if(strlen($s_item)>50) $s_item=substr($s_item,0,47)."...";
			echo htmlspecialchars($s_item . $extra );
			if( $is_readable ) {
				echo "</a>";	// ...$extra...
			}
			echo "</td>\n";
			// Size
			echo "<td>".parse_file_size(get_file_size( $abs_item))."</td>\n";
			// type
			echo "<td>".get_mime_type( $abs_item, "type")."</td>\n";
			// modified
			echo "<td>".parse_file_date( get_file_date($abs_item) )."</td>\n";
			// permissions
			echo "<td>";
	
			if($allow && $is_chmodable) {
				echo "<a href=\"".make_link("chmod",$dir,$item)."\" title=\"";
				echo $GLOBALS["messages"]["permlink"]."\">";
			}
	
			$perms = get_file_perms( $abs_item );
			if( strlen($perms)>3) {
				$perms = substr( $perms, 2 );
			}
			echo '<strong>'.$perms.'</strong><br />'
			.parse_file_type($dir,$item)
			.parse_file_perms( $perms )
			;
			if($allow && $is_chmodable ) {
				echo "</a>";
			}
			echo "</td>\n";
	
			// Owner
			error_reporting( E_ALL );
			if( extension_loaded( "posix" )) {
				echo "<td>\n";
				if(function_exists('posix_getpwuid'))
				{
					$user_info = posix_getpwuid( $file_info["uid"] );
					$group_info = posix_getgrgid($file_info["gid"] );
					echo $user_info["name"]. " (".$file_info["uid"].") /<br/>";
					echo $group_info["name"]. " (".$file_info["gid"].")";
				}
				else
				{
					echo "UID: (".$file_info["uid"].") /<br/>";
					echo "GID: (".$file_info["gid"].")";
				}
				echo "</td>\n";
			}
			// actions
			echo "<td style=\"white-space:nowrap;\">\n";
	
			// Rename
			// A file that could be deleted can also be renamed
			if($allow && $is_deletable) {
				echo "<a href=\"".make_link("rename",$dir,$item)."\">";
				echo "<img border=\"0\" width=\"16\" height=\"16\" ";
				echo "src=\"".OSEFILEMAN_B_URL."/assets/images/drive_rename.png\" alt=\"".$GLOBALS["messages"]["renamelink"]."\" title=\"";
				echo $GLOBALS["messages"]["renamelink"]."\" /></a>\n";
			}
			else {
				echo "<img border=\"0\" width=\"16\" height=\"16\" ";
				echo "src=\"".OSEFILEMAN_B_URL."/assets/images/drive_rename.png\" alt=\"".$GLOBALS["messages"]["renamelink"]."\" title=\"";
				echo $GLOBALS["messages"]["renamelink"]."\" />\n";
			}
	
			// EDIT
	
			if(get_is_editable($abs_item)) {
	
				if($allow && $is_writable) {
					echo "<a href=\"".make_link("edit",$dir,$item)."\">";
					echo "<img border=\"0\" width=\"16\" height=\"16\" ";
					echo "src=\"".OSEFILEMAN_B_URL."/assets/images/script_code_red.png\" alt=\"".$GLOBALS["messages"]["editlink"]."\" title=\"";
					echo $GLOBALS["messages"]["editlink"]."\" /></a>\n";
				}
				else {
					echo "<img border=\"0\" width=\"16\" height=\"16\" ";
					echo "src=\"".OSEFILEMAN_B_URL."/assets/images/script_code_red.png\" alt=\"".$GLOBALS["messages"]["editlink"]."\" title=\"";
					echo $GLOBALS["messages"]["editlink"]."\" />\n";
				}
			} else {
				// Extract Link
				if( nx_isArchive( $item ) && !nx_isFTPMode() ) {
					echo "<a ";
					echo "onclick=\"javascript: ClearAll();  Ext.Msg.confirm('Notice','".$GLOBALS["messages"]["extract_warning"]."',function(btn,txt)	{ if(btn == 'yes')	{var dirSelected = document.getElementById('dir');  var fileSelected = document.getElementById('item_$i'); ajaxAction('com_ose_fileman', 'files', 'extract_item', dirSelected.value, fileSelected.value); }  else { return false;} });\" ";
					echo "href=\"#\" title=\"".$GLOBALS["messages"]["extractlink"]."\">";
					echo "<img border=\"0\" width=\"16\" height=\"16\" ";
					echo "src=\"".OSEFILEMAN_B_URL."/assets/images/package_go.png\" alt=\"".$GLOBALS["messages"]["extractlink"];
					echo "\" title=\"".$GLOBALS["messages"]["extractlink"]."\" /></a>\n";
				}
				else {
					//echo "<img border=\"0\" width=\"16\" height=\"16\" ";
					//echo "src=\"".OSEFILEMAN_B_URL."/assets/images/_.gif\" alt=\"\" />\n";
				}
			}
			// VIEW
			if( get_is_editable($abs_item) && $GLOBALS['nx_File']->is_readable( $abs_item ) && get_is_file( $abs_item)) {
				$link = make_link("codes",$dir,$item).'&tmpl=component';
				$status = 'status=no,toolbar=no,scrollbars=0,titlebar=no,menubar=no,resizable=yes,width=750,height=350,directories=no,location=no,screenX=100,screenY=100';
				echo "<a href=\"".$link."\" onclick=\"window.open('$link','win2','$status'); return false;\" title=\"". $GLOBALS["messages"]["viewlink"]."\">";
				echo "<img border=\"0\" width=\"16\" height=\"16\" ";
				echo "src=\"".OSEFILEMAN_B_URL."/assets/images/magnifier.png\" alt=\"".$GLOBALS["messages"]["viewlink"]."\" /></a>\n";
			}
			// DOWNLOAD / Extract
			if(get_is_file( $abs_item )) {
				if($allow) {
					echo "<a href=\"".make_link("download",$dir,$item)."\" title=\"".$GLOBALS["messages"]["downlink"]."\">";
					echo "<img border=\"0\" width=\"16\" height=\"16\" ";
					echo "src=\"".OSEFILEMAN_B_URL."/assets/images/drive_web.png\" alt=\"".$GLOBALS["messages"]["downlink"];
					echo "\" title=\"".$GLOBALS["messages"]["downlink"]."\" /></a>\n";
				} else if(!$allow) {
					echo "<td><img border=\"0\" width=\"16\" height=\"16\" ";
					echo "src=\"".OSEFILEMAN_B_URL."/assets/images/drive_web.png\" alt=\"".$GLOBALS["messages"]["downlink"];
					echo "\" title=\"".$GLOBALS["messages"]["downlink"]."\" />\n";
				}
			} else {
				//echo "<img border=\"0\" width=\"16\" height=\"16\" ";
				//echo "src=\"".OSEFILEMAN_B_URL."/assets/images/_.gif\" alt=\"\" />\n";
			}
			// DELETE
			//if(get_is_file( $abs_item)) {
				if($allow && $GLOBALS['nx_File']->is_deletable( $abs_item )) {
					$confirm_msg = sprintf($GLOBALS["messages"]["confirm_delete_file"], $item );
					echo "<a name=\"link_item_$i\" href=\"#link_item_$i\" title=\"".$GLOBALS["messages"]["dellink"]."\"
					onclick=\"javascript: ClearAll(); document.getElementById('item_$i').checked = true;  Ext.Msg.confirm('Notice','".$confirm_msg."',function(btn,txt)	{ if(btn == 'yes')	{var dirSelected = document.getElementById('dir');  var fileSelected = document.getElementById('item_$i'); ajaxAction('com_ose_fileman', 'files', 'del_item', dirSelected.value, fileSelected.value);  }   else {   document.getElementById('item_$i').checked = false; return false;} 	});\">";
					echo "<img border=\"0\" width=\"16\" height=\"16\" ";
					echo "src=\"".OSEFILEMAN_B_URL."/assets/images/delete.png\" alt=\"".$GLOBALS["messages"]["dellink"];
					echo "\" title=\"".$GLOBALS["messages"]["dellink"]."\" /></a>\n";
				}
				//else {
					//echo "<img border=\"0\" width=\"16\" height=\"16\" ";
					//echo "src=\"".OSEFILEMAN_B_URL."/assets/images/cross.png\" alt=\"".$GLOBALS["messages"]["dellink"];
					//echo "\" title=\"".$GLOBALS["messages"]["dellink"]."\" />\n";
				//}
			//} else {
				//echo "<img border=\"0\" width=\"16\" height=\"16\" ";
				//echo "src=\"".OSEFILEMAN_B_URL."/assets/images/_.gif\" alt=\"\" />\n";
			//}
			echo "</td></tr>\n";
			$i++;
		}
	}
	
	
}
?>