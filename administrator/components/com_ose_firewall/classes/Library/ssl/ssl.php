<?php
/**
 * Created by PhpStorm.
 * User: suraj
 * Date: 26/09/2016
 * Time: 2:53 PM
 */
if (!defined('OSE_FRAMEWORK') && !defined('OSEFWDIR') && !defined('_JEXEC'))
{
    die('Direct Access Not Allowed');
}
define('TEST_DOMAIN','http://joomla-membership.com');
define('TEST_SSL',true);
oseFirewall::loadJSON ();
class oseSslcert {
    private $domain = '';
    private $webKey = '';
    private $email = null;
    private $ip = null;
    private $orderBy = null;
    private $limitStm = null;
    private $where = null;

    public function __construct()
    {
        $this->db = oseFirewall::getDBO();
        $this->accessIp();
    }

    public  function get_domain()
    {
        if(TEST_SSL)
        {
          $pieces = parse_url(TEST_DOMAIN);
        }else
        {
          $url = 'http://'. $_SERVER['HTTP_HOST'];
          $pieces = parse_url($url);
        }
        $domain = isset($pieces['host']) ? $pieces['host'] : '';
        if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) {
            return $regs['domain'];
        }
        return false;
    }

    /*
     * check if the domain is validated
     *  status 0 =>error
     * 1=>the domain is validated
     * 2=>domain not validated
     * 3=> re validate the domain
     */
    public function isDomainValidated()
    {
        if(TEST_SSL)
        {
            return oseFirewallBase::prepareSuccessMessage('The domain has been validated');
//            return oseFirewallBase::prepareCustomMessage(2,'The domain is not validated');
//            return oseFirewallBase::prepareCustomMessage(3,'Re validate the domain');
        }
        $domain = $this->get_domain();
        if($domain == false)
        {
            return oseFirewallBase::prepareErrorMessage('The domain is not accessible');
        }
        $this->webKey = oseFirewallBase::getWebKey();
        if(!empty( $this->webKey))
        {
            $temp = $this->sendCheckDomainStatusRequest($domain,$this->webKey);
            $response = json_decode($temp);
           if($response->success == 1 && $response->status == 'Success')
           {
               if(isset($response->message) && ($response->message == "Domain hasn's been validated yet"))
               {
                   return oseFirewallBase::prepareCustomMessage(2,'The domain is not validated');
               }

               if(isset($response->message) && ($response->message == "Domain has been validated"))
               {
                   $dbentryExists = $this->getDomainInfoFromDb($domain);
                   if(!empty($dbentryExists))
                   {
                       return oseFirewallBase::prepareSuccessMessage('The domain has been validated');
                   }else {
                       return oseFirewallBase::prepareCustomMessage(3,'Re validate the domain');
                   }

               }
           }
        }
        else
        {
            return oseFirewallBase::prepareErrorMessage('Please activate the membership to use this feature');
        }


    }

    public function sendCheckDomainStatusRequest($domain,$webkey)
    {
        $url = API_SERVER."sslcerts/checkDomainStatus";
        $content['webKey'] = $webkey;
        $content['domain'] = $domain;
        oseFirewallBase::loadLibClass('panel','panel');
        $panel = new panel();
        $response = $panel->sendRequestReturnRes($content,$url);
        return $response;
    }


    /*
     * STEP 1 : VALIDATE THE DOMAIN
     * SEND PARAMTERS  : WEBKEY AND THE DOMAIN NAME TO THE API
     */
    public function queryEmailForValidation()
    {
        $this->webKey = oseFirewallBase::getWebKey();
        if(!empty( $this->webKey))
        {
            $this->domain = $this->get_domain();
            if(!empty($this->domain))
            {
                $temp = $this->sendqueryEmailForValidationRequest($this->domain,$this->webKey);
                $response = json_decode($temp);
                $result =  $this->checkErrorResponse($response,'validating the email');
                return $result;
            }else {
                return oseFirewallBase::prepareErrorMessage('There was some problem in getting the domain name ');
            }
        }else {
            return oseFirewallBase::prepareErrorMessage('Please activate the membership to use this feature');
        }
    }

    //PREPARE THE CURL REQUEST TO VALIDATE A DOMAIN
    //RETRURNS A LIST OF VALID EMAILS FOR A DOMAIN
    public function sendqueryEmailForValidationRequest($domain,$webkey)
    {
        $url = API_SERVER."sslcerts/queryEmailForValidation";
        $content['webKey'] = $webkey;
        $content['domain'] = $domain;
        oseFirewallBase::loadLibClass('panel','panel');
        $panel = new panel();
        $response = $panel->sendRequestReturnRes($content,$url);
        return $response;
    }

    /*
     * STEP 2 : GET THE VERIFICATION CODE FOR A DOMAIN
     * AN EMAIL WILL BE SENT WITH THE AUTHENTICATION CODE ON THE SELECTED EMAIL ID
     */
    public function sendVerificationCode($email)
    {
        $this->email = $email;
        $validated = filter_var($email, FILTER_VALIDATE_EMAIL);
        $this->webKey = oseFirewallBase::getWebKey();
        $this->domain = $this->get_domain();
        if(empty($email) || $validated == false)
        {
           return  oseFirewallBase::prepareErrorMessage('Please insert a valid email address');
        }else {
            if(empty($this->domain) || empty($this->webKey))
            {
                return oseFirewallBase::prepareErrorMessage('Domain and the web Key are null');
            }else {
                $temp = $this->sendVerificationCodeRequest($email,$this->webKey,$this->domain);
                $response = json_decode($temp);
                $result = $this->checkErrorResponse($response, 'sending the verfication code');
                if($result['status'] == 1)
                {
                    $this->storeEmailTemp($email);
                }
                return $result;
            }
        }
    }

    //CURL REQUEST TO ASK API TO SEND A VALIDATION EMAIL TO SELECTED EMAIL ID
    public function sendVerificationCodeRequest($email,$webkey,$domain)
    {
        $url = API_SERVER."sslcerts/sendVerificationCode";
        $content['webKey'] = $webkey;
        $content['domain'] = $domain;
        $content['email'] = $email;
        oseFirewallBase::loadLibClass('panel','panel');
        $panel = new panel();
        $response = $panel->sendRequestReturnRes($content,$url);
        return $response;
    }

    /*
     * STEP 3 : ACCEPT THE AUTHENTICATION FOR THE USER AND AUTHENTICATE A DOMAAIN
     *
     */
    public function validateDomain($authcode)
    {
        $authcode = oseFirewallBase::cleanupVar($authcode);
        $email = $this->getEmailFromTempFile();
        if($email == false)
        {
            return oseFirewallBase::prepareErrorMessage('There was some problem in retrieving th email address, Please Try Again');
        }
        $validated = filter_var($email, FILTER_VALIDATE_EMAIL);
        if($validated == false || empty($validated))
        {
            return oseFirewallBase::prepareErrorMessage('Please insert a valid email address ');
        }
        $this->webKey = oseFirewallBase::getWebKey();
        $this->domain = $this->get_domain();
        $this->accessIp();
        if(empty($authcode) || empty($email) || empty($this->domain) || empty($this->webKey) || empty($this->ip))
        {
            return oseFirewallBase::prepareErrorMessage('The parameters for the request are empty');
        }else {
            $temp = $this->sendValidateDomainRequest($email,$this->webKey,$this->domain,$authcode,$this->ip);
            $response = json_decode($temp);
            if(!empty($response) && $response->status == 1 && $response->shortMsg == 'success')
            {
                if(isset($response->info) && $response->info !== false)
                {
                    $result =  $this->updateDomainValidationInfo($response->info);
                    if(file_exists(OSE_SSL_TEMP_FILE))
                    {
                        unlink(OSE_SSL_TEMP_FILE);
                    }
                    return $result;
                }
                else {
                    if(isset($response->info) && $response->info == false)
                    {
                        return oseFirewallBase::prepareSuccessMessage('The Domain was validated successfully but the database has been not updated');
                    }else {
                        $result = oseFirewallBase::prepareSuccessMessage('The domain has already been validated ');
                        return $result;
                    }
                }
            }else {
//                return oseFirewallBase::prepareErrorMessage('There was some problem in validating the domain <br/>');
                return oseFirewallBase::prepareCustomDetailedMessage(0,'There was some problem in validating the domain',(string)$response);
            }
        }
    }

    //CURL REQUEST TO SEND THE AUTHENTICATION CODE TO THE API AND GET THE DOMAIN VALIDATED
    public function sendValidateDomainRequest($email,$webkey,$domain,$authcode,$ip)
    {
        $url = API_SERVER . "sslcerts/validateDomain";
        $content['webKey'] = $webkey;
        $content['domain'] = $domain;
        $content['email'] = $email;
        $content['authcode'] = $authcode;
        $content['ip'] = $ip;
        oseFirewallBase::loadLibClass('panel', 'panel');
        $panel = new panel();
        $response = $panel->sendRequestReturnRes($content, $url);
        return $response;
    }

    public function updateDomainValidationInfo($response)
    {
        if(!empty($response) && !empty($response->domain) && !empty($response->order_id) && !empty($response->verified) && !empty($response->qualified_email) && !empty($response->date)){
            if (!$this->domainExists($response->domain)) {
                $varArray = array(
                    'domain' => $response->domain,
                    'email' => $response->qualified_email,
                    'validation_date' => $response->date,
                );
                $id = $this->db->addData('insert', '#__osefirewall_domain_validation', null, null, $varArray);
                if ($id !== 0) {
                    return oseFirewallBase::prepareSuccessMessage('The Domain has been Validated Successfully ');
                } else {
                    return oseFirewallBase::prepareErrorMessage('There was some problem in updating the database for the domain validation ');
                }
            } else {
                $varArray = array(
                    'validation_date' => $response->date,
                );
                $id = $this->db->addData('update', '#__osefirewall_domain_validation', 'domain', $response->domain, $varArray);
                if ($id !== 0) {
                    return oseFirewallBase::prepareSuccessMessage('The validated domain has been updated  ');
                } else {
                    return oseFirewallBase::prepareErrorMessage('There was some problem in updating the database for the validated domain ');
                }
            }
        }
        else
        {
           //cannot update the db as the databse entry for the tables is empty
            return oseFirewallBase::prepareErrorMessage('The paramters to store the validation records are empty');
        }
    }

    //check if the domain has been validated
    public function domainExists($domain)
    {
        $query = 'SELECT COUNT(`id`) AS count FROM `#__osefirewall_domain_validation` WHERE `domain` ='.$this->db->quoteValue($domain);
        $this->db->setQuery($query);
        $temp = $this->db->loadObject();
        if($temp->count>0)
        {
            return true;
        }else {
            return false;
        }
    }

    //CHECK IF RESPONSE HAS ANY ERROR AND INFORM THE USER
    public function checkErrorResponse($response,$type)
    {

        if(isset($response->status) && $response->status == 'Error')
        {
            if(isset($response->message))
            {
                return oseFirewallBase::prepareErrorMessage($response->message);
            }else {
                return oseFirewallBase::prepareErrorMessage('There was some problem in '.$type);
            }
        }else {
            if($type == 'sending the verfication code')
            {
                if(isset($response->message) && $response->message== 'Domain has been validated') {
                    return oseFirewallBase::prepareSuccessMessage('The domain has already been validated ,'.CONTACT_SUPPORT);
                }else {
                    return oseFirewallBase::prepareSuccessMessage($response->message);
                }
            } else{
                $result = array();
                $convertedt_Array = (array) $response;
                if(!empty($convertedt_Array))
                {
                    foreach($convertedt_Array as $record)
                    {
                        array_push($result,$record);
                    }
                }
                return oseFirewallBase::prepareSuccessMessage($result);
            }

        }
    }

    //returns the complte domain
    public function getCompleteDomain()
    {
        if(TEST_SSL)
        {
            $parse = parse_url(TEST_DOMAIN);
        }else {
            $url = "http://".$_SERVER['HTTP_HOST'];
            $parse = parse_url($url);
        }
        if(!empty($parse['host']))
        {
            return $parse['host'];
        }else {
            return false;
        }
    }

    //STEP4 : GENERATE SSL CERTIFICATE
    public function generateSSLCertificate($data)
    {
        $cleanedVariables = $this->cleanVariables($data);
        $this->accessIp();
        if($cleanedVariables == false)
        {
            return oseFirewallBase::prepareErrorMessage('Please fill in all the fields ');
        }
        else
        {
            $domain_temp = $this->getCompleteDomain();
            if($domain_temp == false)
            {
                return oseFirewallBase::prepareErrorMessage('The domain name cannot be accessed ');
            }else
            {
                $cleanedVariables['domain'] = $domain_temp; //common name  can be sub domain as well
            }
            $cleanedVariables['webKey'] = oseFirewallBase::getWebKey();
            $cleanedVariables['ip'] = $this->ip;
            $temp =  $this->sendGenSSLCertRequest($cleanedVariables);
            $response = json_decode($temp);
//            echo '<pre>';
//            print_r($response);
//            echo '<pre>';
//            exit;
            if(TEST_SSL) {
                //TODO TESTIN CODE
            $response = new stdClass();
            $response->success = 1;
            $response->status = 'Success';
            $response->message = 'The SSL certificate has been issued';
            $response->ssl_order_id = '11ac862f-784a-4036-a1a3-d77d4c60eb2e';
                //TODO : TEST CODE ENDS
            }
            if ($response->status == 'Success' && $response->success == 1) {
                if (!empty($response->ssl_order_id)) {
                    $sslcertdb_update = $this->updateSSLCertiInfo((string)$response->ssl_order_id,$cleanedVariables['domain']);
                    if ($sslcertdb_update['status'] == 1) {
                        //download  the certificate
//                        $this->retrieveSSLCertificate();
                        return $sslcertdb_update;

                    } else {
                        //error in updating the ssl cert db
                        return $sslcertdb_update;
                    }
                } else {
                    return oseFirewallBase::prepareErrorMessage('The SSL Certificate was generated successfully but the returned ssl_order_id from server is empty');
                }
                //certificate was generated successfully
                //update the db for the ssl certificate
                //prepare to download the certificate
            } else {
                //error in certificate generation from the api
//                return oseFirewallBase::prepareErrorMessage('There was some problem in generating the SSL Certificate, Please try again or contact support team at support@centrora.com');
                return oseFirewallBase::prepareCustomDetailedMessage(0,'There was some problem in generating the SSL Certificate, Please try again or contact support team at support@centrora.com',$response);

            }
        }
    }

    public function sendGenSSLCertRequest($data)
    {
        $url = API_SERVER . "sslcerts/requestCerts";
        oseFirewallBase::loadLibClass('panel', 'panel');
        $panel = new panel();
        $response = $panel->sendRequestReturnRes($data, $url);
        return $response;
    }

    public function cleanVariables($temp)
    {
        $result = array();
        if(is_array($temp) && !empty($temp))
        {
            foreach($temp as $key=>$value)
            {
                if(!empty($value)) {
                    $result[$key] = (string)oseFirewallBase::cleanupVar($value);
                }else {
                    return false;
                }
            }
            return $result;
        }
    }

    //update the sslcert database
    public function updateSSLCertiInfo($ssl_order_id,$commonName)
    {
        $domain = $this->get_domain();
        if($domain !== false)
        {
            $domainInfo = $this->getDomainInfoFromDb($domain);
            if($domainInfo !== false)
            {
                //insert the record for new order
                if(!$this->checkIfSSLOrderExists($commonName)) {
                    $varArray = array(
                        'domain_id' => $domainInfo,
                        'ssl_domain' => $commonName,
                        'ssl_order_id' => $ssl_order_id,
                        'issued_date' => date("Y-m-d H:i:s"),
                    );
                    $id = $this->db->addData('insert', '#__osefirewall_sslcert', null, null, $varArray);
                    if ($id !== 0) {
                        return oseFirewallBase::prepareSuccessMessage('The SSL Certificate was generated Successfully ');
                    } else {
                        return oseFirewallBase::prepareErrorMessage('There was some problem in INSERTING the ssl cert database');
                    }
                }else {
                    $varArray = array(
                        'issued_date' => date("Y-m-d H:i:s"),
                        'ssl_order_id' => $ssl_order_id,
                    );
                    $id = $this->db->addData('update', '#__osefirewall_sslcert', 'ssl_oder_id', $ssl_order_id, $varArray);
                    if ($id !== 0) {
                        return oseFirewallBase::prepareSuccessMessage('The SSL Certificate was generated Successfully ');
                    } else {
                        return oseFirewallBase::prepareErrorMessage('There was some problem in UPDATING the ssl cert database');
                    }
                }
            }
            else
            {
                return oseFirewallBase::prepareErrorMessage('The domain '.$domain. 'is not validated, Please validate the domain first ');
            }

        }
        else {
            //domain is empty
            return oseFirewallBase::prepareErrorMessage('Certificate was generated successfully ,But the domain is not accessible');
        }
    }

    public function checkIfSSLOrderExists($commonName)
    {
        $query = 'SELECT * FROM `#__osefirewall_sslcert` WHERE `ssl_domain` = '.$this->db->quoteValue($commonName);
        $this->db->setQuery($query);
        $temp = $this->db->loadObject();
        if(!empty($temp))
        {
            return $temp->id;
        }else {
            return false;
        }
    }



    //get the domain information from the databse
    //the domain needs to be validated
    public function getDomainInfoFromDb($domain)
    {
        $query = 'SELECT * FROM `#__osefirewall_domain_validation` WHERE `domain` ='.$this->db->quoteValue($domain);
        $this->db->setQuery($query);
        $temp = $this->db->loadObject();
        if(!empty($temp))
        {
            return $temp->id;
        }else {
            return false;
        }
    }

    //returns complete information about the valided domain
    public function getDomainInfo($domain)
    {
        $query = 'SELECT * FROM `#__osefirewall_domain_validation` WHERE `domain` ='.$this->db->quoteValue($domain);
        $this->db->setQuery($query);
        $temp = $this->db->loadObject();
        if(!empty($temp))
        {
            return $temp;
        }else {
            return false;
        }
    }

    //returns the order id for the certificate
    public function getSSLOrderId($domain)
    {
        if(!empty($domain)) {
            $query = 'SELECT * FROM `#__osefirewall_sslcert` WHERE `ssl_domain` = '.$this->db->quoteValue($domain);
            $this->db->setQuery($query);
            $temp = $this->db->loadObject();
            if (!empty($temp)) {
                return $temp->ssl_order_id;
            } else {
                return false;
            }
        }else {
            return false;
        }
    }

    //get the server form the api
    public function retrieveSSLCertificate()
    {
        $data = array();
        $this->accessIp();
        $domain = $this->getCompleteDomain();
        $data['ssl_order_id'] = $this->getSSLOrderId($domain);
        $data['ip'] = $this->ip;
        $data['domain'] = $this->get_domain();
        if(empty($data['domain']))
        {
            return oseFirewallBase::prepareErrorMessage('The root domain canno be accessed , Please try again!');
        }
        if(empty($data['ip']))
        {
            return oseFirewallBase::prepareErrorMessage('The IP cannot be accessed, Please try again! ');
        }
        if($data['ssl_order_id'] !==false)
        {
            //get the certificate content from the server
           $temp =  $this->sendRetrieveSSLCertRequest($data);
            if(TEST_SSL)
            {
                $test =json_decode($temp);
                $response  = json_decode($test);
            }else {
                $test =json_decode($temp);
                $response  = json_decode($test);
            }
            if($response->status == 1 && $response->shortMsg == 'success')
            {
                //successfully retrieved the certificate content
                $prepareContent = $this->prepareSSLCertificateDownloadContent($response);
                if($prepareContent['status'] == 1)
                {
                    //prepare the zip and download it
                    $zip_result = $this->prepareCertificateZip();
                    return $zip_result;
                }else {
                    //error in storing the certificates content locally
                    return $prepareContent;
                }
            }else {
                //failed to retrive the certificate
                if(isset($response->shortMsg))
                {
                    return oseFirewallBase::prepareErrorMessage('Failed to retrieve certificate : '.$response->shortMsg);
                }else {
                    return oseFirewallBase::prepareErrorMessage('Failed to retrieve the certificate, Please contact the support team at support@centrora.com');
                }
            }
        }else {
            return oseFirewallBase::prepareErrorMessage('The ssl order id is blank ');
        }
    }

    public function sendRetrieveSSLCertRequest($data)
    {
        $url = API_SERVER . "sslcerts/retrieveCerts";
        oseFirewallBase::loadLibClass('panel', 'panel');
        $panel = new panel();
        $response = $panel->sendRequestReturnRes($data, $url);
        return $response;
    }

    public function prepareSSLCertificateDownloadContent($content)
    {
        if(isset($content->data->intermediateCertificate) && isset($content->data->certificate)) {
            $intermediateCertificate = $content->data->intermediateCertificate;
            $certificate = $content->data->certificate;
            if (!file_exists(OSE_SSL_CERT_FOLDER)) {
                mkdir(OSE_SSL_CERT_FOLDER);
            }
            //delete the existing certificates
            if (file_exists(OSE_SSL_CERTIFICATE)) {
                unlink(OSE_SSL_CERTIFICATE);
            }
            if (file_exists(OSE_SSL_INTERMEDIATE_CERT)) {
                unlink(OSE_SSL_INTERMEDIATE_CERT);
            }
            $inter_file_result = $this->writeFiles($intermediateCertificate, OSE_SSL_INTERMEDIATE_CERT);
            $cert_file_result = $this->writeFiles($certificate, OSE_SSL_CERTIFICATE);
            if ($cert_file_result && $inter_file_result) {
                return oseFirewallBase::prepareSuccessMessage('The certificate have been saved locally');
            } else {
                return oseFirewallBase::prepareErrorMessage('There was some error in saving the certificates locally');
            }
        }else {
            return oseFirewallBase::prepareErrorMessage('The response contetn is blank');
        }
    }

    public function writeFiles($content,$filename)
    {
        $result = file_put_contents($filename, $content);
        return ($result == false) ? false : true;
    }

    public function prepareCertificateZip()
    {
        $certificates = $this->getFileContent();
        if(isset($certificates['status']) && $certificates['status'] == 0)
        {
            //certificates are not stored locally
            return $certificates;
        }else if(!empty($certificates['interm']) && !empty($certificates['cert'])){
            $domain = $this->get_domain();
            if ($domain == false) {
                $domain = 'SSLCertificates';
            }
            $archive_file_name = $domain . '.zip';
            $file = OSE_SSL_CERT_FOLDER . ODS . $archive_file_name;
            $fp = fopen($file, "a");
            fclose($fp);
            $zip = new ZipArchive;
            $res = $zip->open($file, ZipArchive::CREATE);
            if ($res === TRUE) {
                $zip->addFromString('1_root_bundle.crt', base64_decode($certificates['interm']));
                $zip->addFromString('2_protect-website.com.crt', base64_decode($certificates['cert']));
                $zip->close();
//                $result = $this->downloadSSLCertZip($file,$archive_file_name);
//                return $result;
                return oseFirewallBase::prepareSuccessMessage(SSL_ZIP_DOWNLOAD_URL);
            } else {
                return oseFirewallBase::prepareErrorMessage('There was some problem in generating the zip of the certificates ');
            }
        }

    }

    public function downloadSSLCertZip($file,$archive_file_name)
    {
        if (file_exists($file))
        {
            header('Content-Description:     File Transfer');
            header('Content-Type: application/force-download' );
            header('Content-Disposition: attachment; filename="' . $archive_file_name.'"');
            header('Content-Transfer-Encoding: binary');
            header("Cache-control: private");
            header('Pragma: private');
            header('Content-Length: ' . filesize($file));
            while (ob_get_level()) {
                ob_end_clean();
            }
            readfile($file);
            exit;
        }
        else
        {
            print_r("Certificates Zip Does Not Exists");
            exit;
        }
    }

    //download the existing local files which are still valid
    public function downloadLocalSSLFiles()
    {
        $domain = $this->get_domain();
        if($domain == false)
        {
            return oseFirewallBase::prepareErrorMessage('The domain name is not accessible');
        }
        $archive_file_name = $domain . '.zip';
        $file = OSE_SSL_CERT_FOLDER . ODS . $archive_file_name;
         $this->downloadSSLCertZip($file,$archive_file_name);
//        return $result;
    }

    public function getFileContent()
    {
        $content  = array();
        if(file_exists(OSE_SSL_INTERMEDIATE_CERT))
        {
            $content['interm'] = file_get_contents(OSE_SSL_INTERMEDIATE_CERT);
        }else {
            return oseFirewallBase::prepareErrorMessage('The local copy of the intermediate certificate does not exists');
        }
        if(file_exists(OSE_SSL_CERT_FOLDER))
        {
            $content['cert'] = file_get_contents(OSE_SSL_CERTIFICATE);
        }else{
            return oseFirewallBase::prepareErrorMessage('The local copy of the certificate does not exists');
        }
        return $content;
    }
    public function getdomains()
    {
        $data['root'] = $this->get_domain();
        $data['sub'] = $this->getCompleteDomain();
        if($data['root'] == false)
        {
            return oseFirewallBase::prepareErrorMessage('There was some problem in accessing the domain ');
        }else {
            $result['status'] = 1;
            $result['subdomain'] =  $data['sub'] ;
            $result['root'] = $data['root'];
            return $result;
        }
    }

    public function checkSSLCert()
    {
        if(TEST_SSL)
        {
            return oseFirewallBase::prepareSuccessMessage('Valid Certificates Exists, No need to generate new certificates ');
//            return oseFirewallBase::prepareErrorMessage('Generate new certificates ');
        }
//        $zipExists = $this->certificatesZipExists();
        $notExpired = $this->sslCertNotExpired();
        if($notExpired == true)
        {
            return oseFirewallBase::prepareSuccessMessage('Valid Certificates Exists, No need to generate new certificates ');
        }else {
            return oseFirewallBase::prepareErrorMessage('Generate new certificates ');
        }
    }


    public function certificatesZipExists()
    {
       $domain = $this->get_domain();
        if($domain == false)
        {
            return false;
        }
        $archive_file_name = $domain . '.zip';

        if(file_exists(OSE_SSL_CERT_FOLDER.ODS.$archive_file_name))
        {
            return true;
        }else {
            return false;
        }
    }

    public function getSSLOrderIssuedDate()
    {
        $domain = $this->getCompleteDomain();
        if(!empty($domain)) {
            $query = 'SELECT * FROM `#__osefirewall_sslcert` WHERE `ssl_domain` = '.$this->db->quoteValue($domain);
            $this->db->setQuery($query);
            $temp = $this->db->loadObject();
            if (!empty($temp)) {
                return $temp->issued_date;
            } else {
                return false;
            }
        }else {
            return false;
        }
    }
    //check if the ssl certificate was generated successfully
    public function sslCertNotExpired()
    {
        $issued_date= $this->getSSLOrderIssuedDate();
        if($issued_date == false)
        {
            return false;
        }else {
            $expired = $this->checkSSLExpiry($issued_date);
            return $expired;
        }
    }

    //check if the ssl is valid
//    ssl certificates expire afetr 3 months
    public function checkSSLExpiry($issued_date)
    {
        $datetime1 = new DateTime();
        $datetime2 = new DateTime($issued_date);
        $interval = $datetime1->diff($datetime2);
        $year_elapsed = $interval->format('%y');
        $months_elapsed = $interval->format('%m');
        if($year_elapsed >= 1 || $months_elapsed >3)
        {
            return false;
        }else {
            return true;
        }
    }

    public function getSSLCertainfo()
    {
        $domain = $this->getCompleteDomain();
        if(!empty($domain)) {
            $query = 'SELECT * FROM `#__osefirewall_sslcert` WHERE `ssl_domain` = '.$this->db->quoteValue($domain);
            $this->db->setQuery($query);
            $temp = $this->db->loadObject();
            if (!empty($temp)) {
                $result['ssl_domain'] = $temp->ssl_domain;
                $result['issued_date'] = $temp->issued_date;
                return oseFirewallBase::prepareSuccessMessage($result);
            } else {
                return oseFirewallBase::prepareErrorMessage('Error in accessing ssl cert info ');
            }
        }else {
            return oseFirewallBase::prepareErrorMessage('Domain is not accessible');
        }
    }

    public function storeEmailTemp($email)
    {
        if(!file_exists(OSE_SSL_CERT_FOLDER))
        {
            mkdir(OSE_SSL_CERT_FOLDER);
        }
        if(file_exists(OSE_SSL_TEMP_FILE))
        {
            unlink(OSE_SSL_TEMP_FILE);
        }
        $content = "<?php\n" . '$email = array("email"=>' . var_export($email, true) .");";
        $this->writeFiles($content, OSE_SSL_TEMP_FILE);
    }

    public function getEmailFromTempFile()
    {
        $email = null;
        require(OSE_SSL_TEMP_FILE);
        if(!empty($email['email']))
        {
            return $email['email'];

        }else {
            return false;
        }
    }

    //IF THE DOMAIN IS VALIDATED BU THE DB DOES NOT HAVE ANY RECORD
    //ASK API TO SEND AN AUTH CODE TO THE VALID EMAIL ADDRESS
    //STEP 3A
    public function sendDomainValidationAuthCode($email)
    {
        $this->email = $email;
        $validated = filter_var($email, FILTER_VALIDATE_EMAIL);
        $this->webKey = oseFirewallBase::getWebKey();
        $this->domain = $this->get_domain();
        $this->accessIp();
        if(empty($this->ip))
        {
            return oseFirewallBase::prepareErrorMessage('The IP Address cannot be accessed ');
        }
        if (empty($validated) || $validated == false) {
            return oseFirewallBase::prepareErrorMessage('Please insert a valid email address');
        } else {
            if (empty($this->domain) || empty($this->webKey)) {
                return oseFirewallBase::prepareErrorMessage('Domain and the web Key are null');
            } else {
//                $temp = $this->sendDomainValidationAuthCodeRequest($email, $this->webKey, $this->domain);
                $temp = $this->sendDomainValidationAuthCodeRequest($email, $this->webKey, $this->domain,$this->ip);
                $response = json_decode($temp);
                if(isset($response) && $response->success == 1 && $response->status == 'Success')
                {
                    if(isset($response->message))
                    {
                        //save the email in a local file
                        $this->storeEmailTemp($email);
                        return oseFirewallBase::prepareSuccessMessage($response->message);
                    }else {
                        return oseFirewallBase::prepareSuccessMessage('Validation code has been sent to the email '.$email);
                    }
                }else {
                    if(isset($response->message))
                    {
                        return oseFirewallBase::prepareErrorMessage($response->message);
                    }else {
                        return oseFirewallBase::prepareErrorMessage('There was some problem in sending the validation code to the email : '.$email);
                    }
                }
            }
        }
    }


    public function sendDomainValidationAuthCodeRequest($email,$webkey,$domain,$ip)
    {
        $url = API_SERVER."sslcerts/sendDomainValidationAuthCode";
        $content['webKey'] = $webkey;
        $content['domain'] = $domain;
        $content['email'] = $email;
        $content['ip'] = $ip;
        oseFirewallBase::loadLibClass('panel','panel');
        $panel = new panel();
        $response = $panel->sendRequestReturnRes($content,$url);
        return $response;
    }

    //STEP 4A : RE VALIDATE THE DOMAIN
    public function revalidateDomain($authcode)
    {
        $authcode = oseFirewallBase::cleanupVar($authcode);
        $this->accessIp();
        if(empty($this->ip))
        {
            return oseFirewallBase::prepareErrorMessage('The IP Address cannot be acceed ');
        }
        $email = $this->getEmailFromTempFile();
        if($email == false)
        {
            return oseFirewallBase::prepareErrorMessage('There was some problem in retrieving th email address, Please Try Again');
        }
        $validated = filter_var($email, FILTER_VALIDATE_EMAIL);
        if($validated == false || empty($validated))
        {
            return oseFirewallBase::prepareErrorMessage('Please insert a valid email address ');
        }
        $this->webKey = oseFirewallBase::getWebKey();
        $this->domain = $this->get_domain();
        if(empty($authcode) || empty($validated) || empty($this->domain) || empty($this->webKey))
        {
            return oseFirewallBase::prepareErrorMessage('The parameters for the request are empty');
        }else {
            $temp = $this->revalidateDomainRequest($validated,$this->webKey,$this->domain,$authcode,$this->ip);
            $response = json_decode($temp);
            if(isset($response->date))
            {
                $result =  $this->updaterevalidatedDomainInfo($this->domain,$validated,$response);
                if(file_exists(OSE_SSL_TEMP_FILE))
                {
                    unlink(OSE_SSL_TEMP_FILE);
                }
                return $result;
            }else  if(!empty($response) && $response->status == "Error")
            {
                if(isset($response->message))
                {
                    return oseFirewallBase::prepareSuccessMessage($response->message);
                }else {
                    $result = oseFirewallBase::prepareSuccessMessage('There was some problem in revalidating the domain');
                    return $result;
                }
            }else {
                    $result = oseFirewallBase::prepareSuccessMessage('There was some problem in re validating the domain');
                    return $result;
            }
        }
    }

    public function revalidateDomainRequest($email,$webkey,$domain,$authcode,$ip)
    {
        $url = API_SERVER . "sslcerts/revalidateDomain";
        $content['webKey'] = $webkey;
        $content['domain'] = $domain;
        $content['email'] = $email;
        $content['authcode'] = $authcode;
        $content['ip'] = $ip;
        oseFirewallBase::loadLibClass('panel', 'panel');
        $panel = new panel();
        $response = $panel->sendRequestReturnRes($content, $url);
        return $response;
    }

    public function updaterevalidatedDomainInfo($domain,$email,$response)
    {
        if(!empty($response) && !empty($domain) && !empty($response->date) && !empty($email)){
            if (!$this->domainExists($domain)) {
                $varArray = array(
                    'domain' => $domain,
                    'email' => $email,
                    'validation_date' => $response->date,
                );
                $id = $this->db->addData('insert', '#__osefirewall_domain_validation', null, null, $varArray);
                if ($id !== 0) {
                    return oseFirewallBase::prepareSuccessMessage('The Domain has been Re - Validated Successfully ');
                } else {
                    return oseFirewallBase::prepareErrorMessage('There was some problem in updating the database for the domain Re validation ');
                }
            } else {
                $varArray = array(
                    'validation_date' => $response->date,
                );
                $id = $this->db->addData('update', '#__osefirewall_domain_validation', 'domain', $response->domain, $varArray);
                if ($id !== 0) {
                    return oseFirewallBase::prepareSuccessMessage('The Re validated domain has been updated  ');
                } else {
                    return oseFirewallBase::prepareErrorMessage('There was some problem in updating the database for the validated Re domain ');
                }
            }
        }
        else
        {
            //cannot update the db as the databse entry for the tables is empty
            return oseFirewallBase::prepareErrorMessage('The paramters to store the validation records are empty');
        }
    }

    public function accessIp()
    {
        if(TEST_SSL)
        {
            $this->ip = "100.100.100.111";
        }else {
            if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                $this->ip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $this->ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                $this->ip = $_SERVER['REMOTE_ADDR'];
            }
        }

    }

    public function getCertificatesTable()
    {
        $columns = oRequest::getVar('columns', null);
        $limit = oRequest::getInt('length', 10);
        $start = oRequest::getInt('start', 0);
        $search = oRequest::getVar('search', null);
        $orderArr = oRequest::getVar('order', null);
        $sortby = null;
        $orderDir = 'asc';
//        if (!empty($columns[1]['search']['value']))
//        {
//            $status = $columns[1]['search']['value'];
//        } else
//        {
//            $status = null;
//        }
        if (!empty($orderArr[0]['column']) || ($orderArr[0]['column'] == 0)) {
            $sortby = $columns[$orderArr[0]['column']]['data'];
            $orderDir = $orderArr[0]['dir'];
        }
        $certficates_list = $this->getCertificateList($search['value'],$start, $limit, $sortby, $orderDir);
        return $certficates_list;
    }

    public function getCertificateList($search,$start, $limit, $sortby, $orderDir)
    {
        $return = array();
        if (!empty($search)) {$this->getWhereName ($search);}
//        if (!empty($status)) {$this->getWhereStatus ($status);}
        $this->getOrderBy($sortby, $orderDir);
        if (!empty($limit)) {
            $this->getLimitStm($start, $limit);
        }
        $where = $this->db->implodeWhere($this->where);
        $return['data'] = $this->getAllRecords($where);
        $count = $this->getAllCountsNoVar($where);
        $return['recordsTotal'] = $count['recordsTotal'];
        $return['recordsFiltered'] = $count['recordsFiltered'];
        return $return;
    }

    protected function getAllRecords($where)
    {
        $sql = 'SELECT `id`, `ssl_domain`,`issued_date` FROM `#__osefirewall_sslcert` ';
        $query = $sql.$where.$this->orderBy." ".$this->limitStm;
        $this->db->setQuery($query);
//        print_r( $this->db->setQuery($query));
        $temp = $this->db->loadResultList();
        $data = $this->convertCertTableContent($temp);
        return $data;

//        echo '<pre>';
//        print_r($temp);
//        echo '<pre>';
//        exit;
    }
    private function getAllCountsNoVar($where)
    {
        $return = array();
        // Get total count
        $query = "SELECT COUNT(`id`) AS count FROM `#__osefirewall_sslcert`";
        $this->db->setQuery($query);
        $result = $this->db->loadObject();
        $return['recordsTotal'] = $result->count;
        // Get filter count
        $this->db->setQuery($query . $where);
        $result = $this->db->loadObject();
        $return['recordsFiltered'] = $result->count;
        return $return;
    }

    protected function getWhereName ($search) {
        $this->where[] = "`ssl_domain` LIKE ".$this->db->quoteValue($search.'%', true);
    }
    protected function getWhereStatus ($status) {
        if ($status == 2)
        {
            $this->where[] = "`status` = 2 ";
        }
        if ($status == 1)
        {
            $this->where[] = "`status` = 1 ";
        }
        if ($status == 3)
        {
            $this->where[] = "`status` = 0 ";
        }

    }
    protected function getOrderBy ($sortby, $orderDir) {
        if (empty($sortby))
        {
            $this->orderBy= " ORDER BY `id` DESC";
        }
        else
        {
            if($sortby == 'datetime')
            {
                $sortby = 'dateadded';
            }
            $this->orderBy= " ORDER BY ".$this->db->quoteKey($sortby).' '.addslashes($orderDir);
        }
    }
    protected function getLimitStm ($start, $limit) {
        if (!empty($limit))
        {
            $this->limitStm = " LIMIT ".(int)$start.", ".(int)$limit;
        }
    }



    public function convertCertTableContent($certificateList)
    {
        $converTedArray = array();
        $i = 0;
        foreach($certificateList as $key=>$value)
        {
          $converTedArray[$key] = $value;
          $converTedArray[$i]['download'] = "<a href='javascript:void(0);' title = 'Inactive' onClick= 'downloadCertificate()' ><i class='text-block glyphicon  glyphicon-download-alt'></i></a>";
          $i ++;
        }
        $data = $converTedArray;
        return $data;
    }

    public function domainValidationRecordExists()
    {
        $domain = $this->get_domain();
        if($domain == false)
        {
            return oseFirewallBase::prepareErrorMessage('The domain name is not accessible ');
        }else {
            $domainInfo = $this->getDomainInfo($domain);
            if(!empty($domainInfo))
            {
                return oseFirewallBase::prepareSuccessMessage('The domain has been validated');
            }else {
                return oseFirewallBase::prepareErrorMessage('The domian has not been validated');
            }
        }
    }

    public function checkAccess($state)
    {
        $accessStates = array();
        $isDomainValidated = $this->isDomainValidated();
        if($isDomainValidated['status'] == 1)
        {
            //domain is validated
            $certExists = $this->checkSSLCert();
            if($certExists['status'] == 0)
            {
                //cert does not exist or have expired
                $accessStates = array('home','state4');
            }else {
                $accessStates = array('home');
            }
        }else {
            //domain is not validated
            $accessStates = array('home','state1','state2','state3');
        }
        if(in_array($state,$accessStates,true))
        {
            return oseFirewallBase::prepareSuccessMessage('Access granted');
        }else {
            return oseFirewallBase::prepareErrorMessage('Access denied');
        }
    }








}